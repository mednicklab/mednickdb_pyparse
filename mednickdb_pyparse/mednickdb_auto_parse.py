import os
import sys
from inspect import signature
import time
import warnings
import logging
from logging import handlers
import queue
import glob
import threading
import requests
from mednickdb_pyapi.pyapi import MednickAPI, ServerError
from typing import Union, List, Dict, Tuple #TODO add type hints to all functions
from mednickdb_pyparse.error_handling import DependencyError, ParseError, ParseWarning
from mednickdb_pysleep.error_handling import EEGError

debug = True if 'ENV' not in os.environ else os.environ['ENV']=='debug'
debug = False

logger = logging.getLogger("errorlog")
formatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
logger.setLevel(logging.DEBUG if debug else logging.INFO)

stream_handler = handlers.RotatingFileHandler("errors.log", maxBytes=1000*1000, backupCount=2)
stream_handler.setLevel(logging.DEBUG if debug else logging.INFO)
stream_handler.setFormatter(formatter)

file_handler = logging.StreamHandler()
file_handler.setFormatter(formatter)
file_handler.setLevel(logging.DEBUG)

#logger.addHandler(file_handler)
logger.addHandler(stream_handler)

logging.basicConfig(
    level=logging.DEBUG if debug else logging.INFO,
    format="%(asctime)s [%(threadName)-14.14s] [%(levelname)-5.5s]  %(message)s",
    handlers=[
        file_handler,
        stream_handler
    ])

print = logger.info

# logging.basicConfig(
#     level=logging.DEBUG if debug else logging.INFO,
#     format="%(asctime)s [%(threadName)-14.14s] [%(levelname)-5.5s]  %(message)s",
#     handlers=[
#         handlers.RotatingFileHandler("errors.log", maxBytes=1000*1000, backupCount=2),
#         logging.StreamHandler()
#     ])


class AutomatedParsingManager:
    """
    The automated data parsing manager class for auto parsing service.
    It:
     - starts a a pool of threads.
     - checks adds file to processing queue
     - adds files to needing dependences queue
     - signals parsing completed for each file
    """

    def __init__(self, parse_rate=3, qsize=0, problem_files=[],
                 compute_heavy_files=('raw_eeg', 'raw_sleep_eeg', 'std_sleep_eeg', 'std_eeg')):
        """
        Constructor of the class.Notice that all parameters could only be configured in the constructor

        :param: max_attempt: integer for the maximum number of consecutive unsuccessful reconnection allowed before
            throwing ConnectionError. The default value is 10
        :param: parse_rate: the interval (second) between each request for unparsed_files. The default value is 3
        :param: qsize: the size of queue for unparsed_files. The default value is 0, meaning no size restriction.
        :param: problems_files: list of problem files that will be skipped directly. The default value is []
        :param: compute_heavy_files: list of fileformats that requires a separated thread to parse.
            The default value is ['raw_eeg', 'raw_sleep_eeg', 'std_sleep_eeg', 'std_eeg']
        """
        self.lock = threading.Lock()
        self.file_in_process = set()
        self.problem_files = set(problem_files)
        self.waiting_files = set() #files that are waiting for dependences. We check these less often.
        self.last_wating_parse = time.time()
        self.last_reporting_time = time.time()
        self.computer_heavy_files = set(compute_heavy_files)
        self.new_file_check_rate = parse_rate
        self.worker_completion_check_rate = 0.25
        self.light_parse_queue = queue.Queue(qsize)
        self.heavy_parse_queue = queue.Queue(qsize)
        self.heavy_workers = []
        self.light_workers = []
        self.status_text = ''
        self.signal_queue = queue.Queue()  #signal back to main that things have finished.
        self.client = None
        self.login()
        self.spawn_threads()
        self.poll_db()

    def login(self):
        """Logins into mednickdb"""
        self.client = MednickAPI(server_address=os.environ['MEDNICKDB_SERVER_ADDRESS'] + ':' + os.environ['MEDNICKDB_API_PORT_NUMBER'],
                                    username='mednickdb.microservices@gmail.com',
                                    password=os.environ['MEDNICKDB_DEFAULT_PW'])

    def spawn_threads(self, heavy_threads=7, light_threads=3):
        while len(self.heavy_workers) < heavy_threads:
            self.heavy_workers.append(
                ParseWorker('HeavyWorker'+str(len(self.heavy_workers)+1), self.heavy_parse_queue, self.signal_queue)) #spawn

        while len(self.light_workers) < light_threads:
            self.light_workers.append(
                ParseWorker('LightWorker'+str(len(self.light_workers)+1), self.light_parse_queue, self.signal_queue)) #spawn


    def check_threads(self):
        self.heavy_workers = [w for w in self.heavy_workers if w.isAlive()]
        self.light_workers = [w for w in self.light_workers if w.isAlive()]
        self.spawn_threads()

    def rm_tmp(self): #TODO check and implement
        """remove temp files form /tmp/ folder"""
        logger.info('Clearing Temp folder')
        files = glob.glob('/tmp/*')
        now = time.time()
        files = [f for f in files if os.path.getmtime(f) < (now - 30*60)]
        for f in files:
            os.remove(f)

    def poll_db(self):
        """
        the enter point of the whole service. It constantly requests unparsed files, prepares files for the parsing algorithm
        (see the documentation of self._preparse_files), and starts parsing subroutines for files that are ready to parse (see
        the documentation of self._process_files).
        """
        logging.info('Beginning polling')
        files_recent_parsed = True
        while True:
            try:
                logging.debug('Getting unparsed files')
                file_infos = self.client.get_unparsed_files(previous_versions=False)  # blocking
                file_infos = self._sort_files_on_fileformat(file_infos)
            except (ConnectionError, IOError):
                logging.error('Connection Error. Sleeping then retrying.')
                time.sleep(5)
                continue
            except ServerError:
                logging.error('Server Error. Token likely expired. Logging in again.')
                # re connect to the server if the token already expired
                self.login()
                continue
            except (Exception, BaseException):
                logger.error('Some unknown error in main. Maybe connection timed out.')
                time.sleep(5)
                continue


            # for prob_file in self.problem_files:
            #     self.client.delete_file(fid=prob_file)

            if len(file_infos) > 0:
                file_infos = self._remove_parsing_files(file_infos)
                file_infos = self._remove_waiting_files(file_infos)
                file_infos = self._remove_problem_files(file_infos)

            if len(file_infos) > 0:  # There are still files to parse or being parsed
                self._enqueue_files(file_infos)

            if len(self.file_in_process) > 0:
                files_recent_parsed = True
                self._check_completion()
                logging.debug('Files processing: ' + str(len(self.file_in_process)))
                time.sleep(self.worker_completion_check_rate)

            if len(self.file_in_process) == 0 and len(file_infos) == 0:
                time.sleep(self.new_file_check_rate)

            if (time.time() - self.last_reporting_time) > 10 and files_recent_parsed:
                self.last_reporting_time = time.time()
                self.check_threads()
                new_status = 'Status: Problem Files:' + str(len(self.problem_files)) \
                             +', Files processing:' + str(len(self.file_in_process)) \
                             +', Files waiting:' + str(len(self.waiting_files))
                if new_status != self.status_text:
                    self.status_text = new_status
                    logging.info(new_status)
                    logging.info('Problem_files:'+str(self.problem_files))
                files_recent_parsed = False

    @staticmethod
    def _sort_files_on_fileformat(files, by_studyid=True):
        fileformat_order = ['study_settings', 'study_description', 'band_power',
                            'sleep_features', 'tabular', 'sleep_scoring', 'scoring_sleep_eeg',
                            'raw_eeg', 'raw_sleep_eeg', 'std_eeg', 'std_sleep_eeg']

        studyid_order = ('mass_ss1', 'mass_ss2', 'mass_ss3', 'mass_ss4', 'mass_ss5',
                         'ucddb', 'wamsley_ken', 'wamsley_r21', 'wamsley_future', 'cfs',
                         'homepap', 'shhs', 'mros', 'sof')

        def sort_func(x):
            major = fileformat_order.index(x['fileformat']) if x['fileformat'] in fileformat_order else -1
            if not by_studyid:
                return major
            else:
                return major + studyid_order.index(x['studyid']) if x['studyid'] in studyid_order else -0.1

        return sorted(files, key=sort_func)


    def _remove_waiting_files(self, files):
        """
        remove all the files waiting for dependences for the list of give files
        :param files: the files to check waiting status for
        :return: files that are not waiting for dependences
        """
        if (time.time() - self.last_wating_parse) > 10:
            if len(self.waiting_files) > 0:
                logging.info('10 seconds elapsed, trying '+str(len(self.waiting_files))+' waiting files again')
                self.waiting_files = set()  # empty waiting files, they will all parse now
            self.last_wating_parse = time.time()
            return files #parse em all!

        non_waiting_files = []
        for file in files:
            if file['_id'] not in self.waiting_files:
                non_waiting_files.append(file)

        return non_waiting_files


    def _remove_parsing_files(self, files):
        non_parsing_files = []
        for file in files:
            if file['_id'] not in self.file_in_process:
                non_parsing_files.append(file)
        return non_parsing_files


    def _remove_problem_files(self, files):
        non_problem_files = []
        for file in files:
            if file['_id'] not in self.problem_files:
                non_problem_files.append(file)
        return non_problem_files


    def _enqueue_files(self, files):
        """
        Adds file to parse queues depending on there computational load

        :param files: list of file information dict
        """
        for file in files:
            logging.debug('Found '+ file['_id']+':'+file['filepath']+', in unparsed files, beginning parse.')
            self.file_in_process.add(file['_id'])
            if file['fileformat'] in self.computer_heavy_files:
                self.heavy_parse_queue.put(file)
            else:
                self.light_parse_queue.put(file)

            logging.debug('{} added to processing list'.format(file['_id']))

    def _check_completion(self):
        """
        Takes a fileinfo from self.input_queue, checks if its file name is in self.problem files. If so, skip it, otherwise,
        checks if the fileformat would take a long time to parse and therefore requires a separated thread. Starts the parsing
        subroutine for the file.(see the description of self._parsing_wrapper). After one file is parsed, this method sends
        output data in self.output_queue to the server if any exists. If the change_status value of the output data is true,
        a request to change 'parsed' field value of a specific record will be sent to the server, and the fid of that file will
        be removed from self.file_in_process, indicating the parsing for the file is completed.
        :return:
        """
        try:
            file_info, error = self.signal_queue.get(False)
        except queue.Empty:
            logging.debug("signal queue empty, no files have finished processing")
        else:
            self.file_in_process.remove(file_info['_id'])
            if error: #deal with errors
                try:
                    raise error
                except DependencyError as e:
                    logging.warning('Could not parse file, dependencies unmet, not setting parsed flag. Error was'+str(e.args[0]))
                    self.waiting_files.add(file_info['_id'])
                except (ParseError, EEGError):
                    logging.exception('Problems parsing file. Problem file was: ' + file_info['filebase'])
                    self.problem_files.add(file_info['_id'])
                    self.spawn_threads()
                except ServerError:
                    self.spawn_threads()
                except (BaseException, Exception):
                    self.problem_files.add(file_info['_id'])
                    logging.exception('Unexpected error in ' + file_info['filebase'])
                    self.spawn_threads()
                    if debug: #reraise error and crash program if in debug mode
                        raise error
            else:
                try:
                    self.client.update_parsed_status(fid=file_info['_id'], status=True)
                except (ConnectionError, ServerError, requests.exceptions.ConnectionError):
                    self.signal_queue.put((file_info, error))  # when disconnected, put result back to the queue.
                    self.login()

            for thread in self.light_workers + self.heavy_workers:
                if thread.lock.locked():
                    thread.lock.release()


class ParseWorker(threading.Thread):
    def __init__(self, name, parse_queue, signal_queue, parse_sleep_time=3):
        super().__init__()
        self.signal_queue = signal_queue
        self.parse_queue = parse_queue
        self.parse_sleep_time = parse_sleep_time
        self.daemon = True
        self.client = None
        self.lock = threading.Lock()
        self.setName(name)
        logging.info('Starting ' + name)
        self.login()
        self.start()

    def login(self):
        """Logins into mednickdb"""
        self.client = MednickAPI(server_address=os.environ['MEDNICKDB_SERVER_ADDRESS'] + ':' + os.environ['MEDNICKDB_API_PORT_NUMBER'],
                                    username='mednickdb.microservices@gmail.com',
                                    password=os.environ['MEDNICKDB_DEFAULT_PW'])

    def run(self):  # batch processing about to work, but the processing flow is awkward
        """
        Takes a fileinfo from self.input_queue, checks if its file name is in self.problem files. If so, skip it, otherwise,
        checks if the fileformat would take a long time to parse and therefore requires a separated thread. Starts the parsing
        subroutine for the file.(see the description of self._parsing_wrapper). After one file is parsed, this method sends
        output data in self.output_queue to the server if any exists. If the change_status value of the output data is true,
        a request to change 'parsed' field value of a specific record will be sent to the server, and the fid of that file will
        be removed from self.file_in_process, indicating the parsing for the file is completed.
        :return:
        """
        from mednickdb_pyparse import parse_dispatcher #importing here will allow for multithreading in MATLAB

        while True:
            # process input files
            try:
                if not self.lock.locked():
                    file_info = self.parse_queue.get(False)
                    self._parse_file(file_info, parse_dispatcher=parse_dispatcher)
            except queue.Empty:
                logging.debug("Nothing to parse")
                time.sleep(self.parse_sleep_time)

    def _parse_file(self, base_file_info, parse_dispatcher):
        """
        Runs the appropriate parsing algorithm based on the file_info (see the description of parse_file_on_fileformat)
        Adds all output data to self.output_queue to send to the server by self.process_files.Each output data is a tuple
        of (output_data, output_file, fid, file_specifier, error, change_status_flag). The change_status_flag is False, unless the output data is
        the last one from its source file. If no output data is generated, output_data and file_specifier fields are None,
        and change_status_flag is set to True, indicating the file is parsed but no data to send back.
        If some error occurs, this is logged but not raised too, so that the regular db can continue as normal.
        TODO: we should probably alert an admin in this case (automatic email, slack webhook)

        :param base_file_info: a dict of file information to parse
        """
        default_specifiers = ['subjectid','versionid','studyid','visitid','sessionid','filetype']

        logging.info('Parsing ' + base_file_info['studyid'] +': '+ base_file_info['filename'] + ' with fid ' + base_file_info['_id'])
        start_time = time.time()
        data_upload_kwargs = set([k for k, v in signature(self.client.upload_data).parameters.items()]) | set(default_specifiers)
        file_upload_kwargs = set([k for k, v in signature(self.client.upload_file).parameters.items()]) | set(default_specifiers)
        base_file_specifiers = {k: v for k, v in base_file_info.items() if k in data_upload_kwargs}
        try:
            datas_out, files_out = parse_dispatcher.dispatch_to_parser(
                med_api=self.client,
                file_specifiers=base_file_specifiers,
                fileformat=base_file_info['fileformat'],
                filepath=base_file_info['filepath'],
                get_files_from_server_storage=True)

            if datas_out is not None: # data to upload
                for idx, data in enumerate(datas_out):
                    # Any fields that are important to data upload need to be copied from existing file:
                    data_keys = list(data.keys()) # because data changes in loop below
                    data_specifiers = {k: v for k, v in base_file_info.items() if k in data_upload_kwargs}
                    data_specifiers.update({k: data.pop(k) for k in data_keys if k in data_upload_kwargs})
                    if 'subjectid' not in data_specifiers:
                        self.signal_queue.put((base_file_info, ParseError("subjectid is required for data upload, and was not provided as file info, or was not present in file: " + base_file_info['filename'])))
                        return
                    logging.debug('Uploading data row '+ str(idx + 1) + ' of ' +\
                          str(len(datas_out)) + ' for ' + base_file_info['filename'])
                    if len(data) > 0:
                        try:
                            self.client.upload_data(data=data, fid=base_file_info['_id'], **data_specifiers)
                        except ServerError:
                            self.signal_queue.put((base_file_info, ServerError))
                            return
                logging.info('Uploaded all data for '+base_file_info['filename'])

            if files_out is not None: # files to upload
                for idx, new_file_info in enumerate(files_out):
                    file_pointer = new_file_info.pop('file')
                    # Any fields that are important to data upload need to be copied from existing file:
                    file_keys = list(new_file_info.keys())  # because data changes in loop below
                    new_file_specifiers = {k: v for k, v in base_file_info.items() if k in file_upload_kwargs and k != '_id'}
                    new_file_specifiers.update({k: new_file_info.pop(k) for k in file_keys if k in file_upload_kwargs})
                    logging.debug('Uploading file '+ str(idx + 1)+ ' of '+\
                          str(len(files_out)) + ' for '+ base_file_info['filename'])
                    try:
                        self.client.upload_file(fileobject=file_pointer, **new_file_specifiers)
                    except ServerError:
                        self.signal_queue.put((base_file_info, ServerError))
                        return

            self.signal_queue.put((base_file_info, None)) #no issues, trigger parsed status change in main
            self.lock.acquire() #block until main has checked this file as processed

        except (ConnectionError, ServerError):
            logging.error('Worker client disconnect')
            self.login()
        except (BaseException, Exception) as error:
            logging.debug('pushing error to main')
            self.signal_queue.put((base_file_info, error))
            return

        end_time = time.time()
        logging.debug('Completed parse for '+base_file_info['filename']+' in '+str(end_time-start_time))


if __name__ == '__main__':
    """Here is the entry to the saclab servers parsing script"""
    parse_rate = 3  # seconds per DB query
    problem_files = []
    manager = AutomatedParsingManager(parse_rate=parse_rate, problem_files=problem_files)

