import yaml
import sys, os
my_path = os.path.abspath('')
sys.path.insert(0,my_path+'/../') #manually add pyparse
sys.path.insert(0,my_path+'/../../mednickdb_pysleep/') #manually add pysleep
from mednickdb_pyparse.parse_scorefile import parse_scorefile

study_settings = yaml.safe_load(open(r'X:\mednicklab_mednickdb\PSTIM\study_settings\PSTIM_study_settings.yaml','r+'))
scorefile_path = r'X:\mednicklab_mednickdb\PSTIM\sleep_scoring\PSTIM_723_V4_NR.mat'

(scoring_data, ), _ = parse_scorefile(scorefile_path, study_settings)