mne==0.18.1
numpy==1.16.4
pandas==0.24.2
pytest==4.6.3
PyYAML==5.1.1
scipy==1.2.1
wonambi==6.01
xlrd==1.2.0
git+https://bitbucket.org/mednicklab/mednickdb_pyapi.git
git+https://bitbucket.org/mednicklab/mednickdb_pysleep.git

