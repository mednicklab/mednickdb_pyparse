import os
from mednickdb_pyapi.pyapi import MednickAPI
import pandas as pd
import tempfile
from mednickdb_pysleep.sleep_features import  assign_stage_to_feature_events, extract_features, detect_slow_osc_spindle_overlap
from mednickdb_pysleep import error_handling, pysleep_utils, sleep_features, sleep_architecture, pysleep_defaults
from mednickdb_pyparse import pyparse_defaults, parse_edf
import numpy as np


def detect_files_with_errors(med_api):
    files_with_errors = []
    files_with_errors.append(detect_bad_spindles(med_api))
    #add more here
    return files_with_errors

def detect_missing_artifacts(med_api):
    all_with_sleep_features = med_api.get_data(query='filetype=sleep_features')
    all_with_sleep_features['has_artifacts'] = all_with_sleep_features['std_sleep_eeg.epochs_with_artifacts'].fillna(False) != False
    all_missing_artifacts = all_with_sleep_features.loc[~all_with_sleep_features['has_artifacts'],:]
    for idx, row in all_missing_artifacts.iterrows():
        if row['studyid'] == 'sof':
            continue
        query = 'studyid='+row['studyid']
        query += ' and subjectid='+str(int(row['subjectid']))
        if not pd.isna(row['visitid']):
            query += ' and visitid='+str(int(row['visitid']))
        file_info_features = med_api.get_files(query+' and fileformat=raw_sleep_eeg')
        if len(file_info_features) == 0:
            print(query, 'is not found')
            continue
        file_info_features = file_info_features[0]
        med_api.delete_file(fid=file_info_features['_id'])


def detect_missing_rem(med_api):
    all_with_sleep_features = med_api.get_data(query='filetype=sleep_features')
    all_with_sleep_features['has_rem'] = all_with_sleep_features['sleep_features.Q4_rem_LOC_rem_event_av_duration'].fillna(False) != False
    all_with_sleep_features['has_rem'] |= all_with_sleep_features['sleep_features.Q3_rem_LOC_rem_event_av_duration'].fillna(False) != False
    all_with_sleep_features['has_rem'] |= all_with_sleep_features['sleep_features.Q2_rem_LOC_rem_event_av_duration'].fillna(False) != False
    all_with_sleep_features['has_rem'] |= all_with_sleep_features['sleep_features.Q1_rem_LOC_rem_event_av_duration'].fillna(False) != False
    all_with_sleep_features['has_rem'] |= all_with_sleep_features['sleep_features.rem_LOC_rem_event_av_duration'].fillna(False) != False
    all_missing_rem = all_with_sleep_features.loc[~all_with_sleep_features['has_rem'],:]
    all_missing_rem = all_missing_rem.reset_index()
    for idx, row in all_missing_rem.iterrows():
        print(idx,'of',all_missing_rem.shape[0])
        query = 'studyid='+row['studyid']
        query += ' and subjectid='+str(int(row['subjectid']))
        if not pd.isna(row['visitid']):
            query += ' and visitid='+str(int(row['visitid']))
        file_info_features = med_api.get_files(query+' and fileformat=sleep_features')
        if len(file_info_features) == 0:
            print(query, 'is not found')
            continue
        file_info_features = file_info_features[0]
        file_bytes = med_api.download_file(fid=file_info_features['_id'])
        f = tempfile.NamedTemporaryFile(suffix='.csv', delete=False)
        f.write(file_bytes)
        do_spindles = False
        try:
            sleep_features_df = pd.read_csv(f.name)
            if 'Unnamed: 0' in sleep_features_df.columns:
                sleep_features_df = sleep_features_df.drop('Unnamed: 0', axis=1)
            if not any(sleep_features_df['description'] == 'rem_event'):
                do_rem=True
            else:
                do_rem=False
            if not any(sleep_features_df['description'] == 'slow_osc'):
                do_slow_osc=True
            else:
                do_slow_osc=False

        except pd.errors.EmptyDataError:
            sleep_features_df = pd.DataFrame()
            do_rem = True
            do_slow_osc = True
            do_spindles =  True

        if (do_rem==False) and (do_spindles==False) and (do_slow_osc==False):
            print('REM, SO, and Spindles are already done. Skipping.')
            continue

        data = med_api.get_data(query, format='flat_dict')[0]
        file_info = med_api.get_files(query=query + ' and fileformat=std_sleep_eeg')
        if len(file_info) > 0:
            file_info = file_info[0]
        else:
            continue
        print('Extracting features... ')
        try:
            rem_df = extract_features(edf_filepath='/data/mednick_server/'+file_info['filepath'],
                                      epochstages=data['sleep_scoring.epochstages'],
                                      epochoffset_secs=data['sleep_scoring.epochoffset_secs'],
                                      do_slow_osc=do_slow_osc,
                                      chans_for_slow_osc=['C3','C4'],
                                      chans_for_spindles=['C3','C4'],
                                      do_spindles=do_spindles,
                                      epochs_with_artifacts=data['std_sleep_eeg.epochs_with_artifacts'],
                                      do_rem=do_rem)
        except error_handling.EEGError:
            print('EEG error')
            continue
        sleep_features_df = pd.concat([sleep_features_df, rem_df], axis=0, sort=False)
        sleep_features_df = sleep_features_df.sort_values(['description','chan','onset'])
        print(sleep_features_df['description'].value_counts())
        base_file_name_parts = os.path.basename(file_info_features['filepath']).split('__')
        base_file_name = '__'.join(base_file_name_parts[:-1])
        std_file_temppath = '/tmp/' + base_file_name + '.csv'
        sleep_features_df.to_csv(std_file_temppath, index=False)
        file_info_features = {k:v for k,v in file_info_features.items() if k in ['studyid','visitid','subjectid','versionid','filetype','fileformat']}
        med_api.upload_file(fileobject=open(std_file_temppath, 'rb'), **file_info_features)

def detect_bad_spindles(med_api):
    all_with_problem = med_api.get_data()
    for idx, row in all_with_problem.iterrows():
        print(idx, 'of', all_with_problem.shape[0])
        query = 'studyid=' + row['studyid']
        query += ' and subjectid=' + str(int(row['subjectid']))
        if not pd.isna(row['visitid']):
            query += ' and visitid=' + str(int(row['visitid']))
        file_info_features = med_api.get_files(query + ' and fileformat=sleep_features')
        if len(file_info_features) == 0:
            print(query, 'is not found')
            continue
        file_info_features = file_info_features[0]
        file_bytes = med_api.download_file(fid=file_info_features['_id'])
        f = tempfile.NamedTemporaryFile(suffix='.csv', delete=False)
        f.write(file_bytes)
        do_spindles = True
        try:
            features_df = pd.read_csv(f.name)
            spindles_df = features_df.loc[features_df['description'] == 'spindle', :]
            features_df = features_df.loc[features_df['description'] != 'spindle', :]
            print('existing spindles', spindles_df.shape[0])
            if 'Unnamed: 0' in features_df.columns:
                features_df = features_df.drop('Unnamed: 0', axis=1)
            if not any(features_df['description'] == 'rem_event'):
                do_rem=True
            else:
                do_rem=False
            if not any(features_df['description'] == 'slow_osc'):
                do_slow_osc=True
            else:
                do_slow_osc=False
        except pd.errors.EmptyDataError:
            features_df = pd.DataFrame()
            spindles_df = pd.DataFrame()
            do_rem = True
            do_slow_osc = True

        data = med_api.get_data(query, format='flat_dict')[0]
        if 'sleep_features.spindle_algo' in data and data['sleep_features.spindle_algo'] == 'Wamsley2012_':
            print('already done')
            continue
        file_info = med_api.get_files(query=query + ' and fileformat=std_sleep_eeg')
        if len(file_info) > 0:
            file_info = file_info[0]
        else:
            continue
        print('Extracting features... ')
        if 'std_sleep_eeg.epochs_with_artifacts' not in data:
            print('Reseting parsing for', query)
            med_api.delete_file(fid=file_info_features['_id'])

            file_info_band_power = med_api.get_files(query + ' and fileformat=band_power')
            if len(file_info_band_power)>0:
                med_api.delete_file(fid=file_info_band_power[0]['_id'])

            med_api.update_parsed_status(fid=file_info['_id'], status=False)
            continue
        try:
            spindles_df = extract_features(edf_filepath='/data/mednick_server/'+file_info['filepath'],
                                      epochstages=data['sleep_scoring.epochstages'],
                                      epochoffset_secs=data['sleep_scoring.epochoffset_secs'],
                                      do_slow_osc=do_slow_osc,
                                      chans_for_slow_osc=['C3'],
                                      chans_for_spindles=['C3'],
                                      do_spindles=do_spindles,
                                      epochs_with_artifacts=data['std_sleep_eeg.epochs_with_artifacts'],
                                      do_rem=do_rem,
                                      spindle_algo='Wamsley2012', #
                                      timeit=True)
        except error_handling.EEGError as e:
            print('unhandled EEG error', e)
            continue
        features_df = pd.concat([features_df, spindles_df], axis=0, sort=False)

        features_df = features_df.sort_values(['description','chan','onset'])
        print(features_df['description'].value_counts())

        features_df = detect_slow_osc_spindle_overlap(features_df,
                                                            coupling_secs=pysleep_defaults.so_spindle_overlap,
                                                            as_bool=True)

        # %% Extract features per quartile, stage, etc
        if features_df is None or features_df.shape[0] == 0:
            sleep_feature_data = {}
        else:
            features_df, _ = pysleep_utils.assign_quartiles(features_df, data['sleep_scoring.epochstages'])
            groupby = ['quartile', 'stage', 'chan', 'description']

            mins_df = sleep_architecture.sleep_stage_architecture(data['sleep_scoring.epochstages'],
                                                                  epochs_to_ignore=data['std_sleep_eeg.epochs_with_artifacts'],
                                                                  return_type='dataframe',
                                                                  per_quartile=True)

            sleep_features_quart_df = features_df.drop(['quartile'], axis=1)
            features_per_stage = sleep_features.sleep_feature_variables_per_stage(features_df,
                                                                                  mins_in_stage_df=mins_df,
                                                                                  av_across_channels=False,
                                                                                  stages_to_consider=pysleep_defaults.stages_to_consider)
            sleep_feature_data = parse_edf.features_per_stage_to_data_dict(features_per_stage, groupby)



            mins_df = sleep_architecture.sleep_stage_architecture(data['sleep_scoring.epochstages'],
                                                                  epochs_to_ignore=data['std_sleep_eeg.epochs_with_artifacts'],
                                                                  return_type='dataframe',
                                                                  per_quartile=False)
            groupby.remove('quartile')
            features_per_stage_no_quart = sleep_features.sleep_feature_variables_per_stage(sleep_features_quart_df,
                                                                                           mins_in_stage_df=mins_df,
                                                                                           av_across_channels=False,
                                                                                           stages_to_consider=pysleep_defaults.stages_to_consider)
            sleep_feature_data_no_quart = parse_edf.features_per_stage_to_data_dict(features_per_stage_no_quart, groupby)

            sleep_feature_data.update(sleep_feature_data_no_quart)


        base_file_name_parts = os.path.basename(file_info_features['filepath']).split('__')
        base_file_name = '__'.join(base_file_name_parts[:-1])
        std_file_temppath = '/tmp/' + base_file_name + '.csv'
        features_df.to_csv(std_file_temppath, index=False)
        file_info_features = {k:v for k,v in file_info_features.items() if k in ['studyid','visitid','subjectid','versionid','filetype','fileformat']}

        print('Uploading files and data')
        med_api.upload_file(fileobject=open(std_file_temppath, 'rb'), **file_info_features)
        file_info_features.pop('fileformat')
        sleep_feature_data['spindle_algo']='Wamsley2012_'
        med_api.upload_data(data=sleep_feature_data, fid=file_info['_id'], **file_info_features)


def detect_rem_offset_error(med_api):
    all_with_problem = med_api.get_data()
    for idx, row in all_with_problem.iterrows():
        print(idx, 'of', all_with_problem.shape[0])
        query = 'studyid=' + row['studyid']
        query += ' and subjectid=' + str(int(row['subjectid']))
        if not pd.isna(row['visitid']):
            query += ' and visitid=' + str(int(row['visitid']))
        file_info_features = med_api.get_files(query + ' and fileformat=sleep_features')
        if len(file_info_features) == 0:
            print(query, 'is not found')
            continue
        file_info_features = file_info_features[0]
        file_bytes = med_api.download_file(fid=file_info_features['_id'])
        f = tempfile.NamedTemporaryFile(suffix='.csv', delete=False)
        f.write(file_bytes)
        try:
            sleep_features_df = pd.read_csv(f.name)
        except pd.errors.EmptyDataError:
            print('bAD DATA')
            continue

        if 'Unnamed: 0' in sleep_features_df.columns:
            sleep_features_df = sleep_features_df.drop('Unnamed: 0', axis=1)
        if any(sleep_features_df.loc[sleep_features_df['description'] == 'rem_event', 'duration']==0.5):
            rem_df = sleep_features_df.loc[sleep_features_df['description'] == 'rem_event',:]
            others_df = sleep_features_df.loc[sleep_features_df['description'] != 'rem_event',:]
            data = med_api.get_data(query, format='flat_dict')[0]
            rem_df = rem_df.drop(['stage','stage_idx'], axis=1)
            rem_df['duration'] = 0.1
            rem_df['onset'] += 0.25 - data['sleep_scoring.lights_off_secs']
            rem_df['chan'] = 'LOC'

            rem_df = assign_stage_to_feature_events(rem_df, data['sleep_scoring.epochstages'])

            assert all(
                rem_df['stage'] == 'rem'), "All stages for rem must be rem. If missmatch maybe epochoffset is incorrect?"

            sleep_features_df = pd.concat([others_df, rem_df], axis=0)
            sleep_features_df = sleep_features_df.sort_values(['description', 'chan', 'onset'])
            print(sleep_features_df['description'].value_counts())
            base_file_name_parts = os.path.basename(file_info_features['filepath']).split('__')
            base_file_name = '__'.join(base_file_name_parts[:-1])
            std_file_temppath = '/tmp/' + base_file_name + '.csv'
            sleep_features_df.to_csv(std_file_temppath, index=False)
            file_info_features = {k: v for k, v in file_info_features.items() if
                                  k in ['studyid', 'visitid', 'subjectid', 'versionid', 'filetype', 'fileformat']}
            med_api.upload_file(fileobject=open(std_file_temppath, 'rb'), **file_info_features)

if __name__ == '__main__':
    med_api = MednickAPI(server_address='http://saclab.ss.uci.edu:8000',
                         username=os.environ['MEDNICKDB_TESTING_EMAIL'],
                         password=os.environ['MEDNICKDB_TESTING_PW'])
    detect_files_with_errors(med_api=med_api)
