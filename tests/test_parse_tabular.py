import sys, os
my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(my_path + '/../mednickdb_pyparse/')
sys.path.append(my_path + '/../mednickdb_pyparse')
sys.path.append(my_path + '/../')
from parse_tabular import parse_surveymonkey_tabular

def test_parse_surveymonkey_tabular():
    file_path = os.path.join(my_path, 'testfiles/example_napi.xlsx')
    parsed_napi_dicts, _ = parse_surveymonkey_tabular(file_path)
    assert all([col.lower() == col for col in parsed_napi_dicts[0]])
    assert all(['?' not in col and '.' not in col and ':' not in col for col in parsed_napi_dicts[0]])
    assert 'subjectid' in parsed_napi_dicts[0]
