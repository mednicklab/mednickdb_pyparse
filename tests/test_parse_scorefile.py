"""tests for python parsing part of mednickdb"""
import sys, os

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(my_path + '/../mednickdb_pyparse/')
sys.path.append(my_path + '/../mednickdb_pyparse')
sys.path.append(my_path + '/../')
from parse_dispatcher import dispatch_to_parser
import datetime


class MockedMednickAPI:
    def __init__(self):
        self.study_settings_path = os.path.join(os.path.dirname(__file__), "testfiles/example1_study_settings.yaml")

    def get_files(self, **kwargs):
        return [{'filepath': self.study_settings_path}]

    def set_study_settings(self, path):
        self.study_settings_path = path


med_api = MockedMednickAPI()


def test_multiline_tabular():
    list_of_datas, _ = dispatch_to_parser(
        med_api=med_api,
        filepath=os.path.join(os.path.dirname(__file__), "testfiles/testtabular1.xlsx"),
        fileformat="tabular")

    s1 = {
        "subjectid": 1,
        "age": 18,
        "sex": "M",
        "bmi": 22
    }

    s2 = {
        "subjectid": 2,
        "age": 22,
        "sex": "F",
        "bmi": 23
    }

    s3 = {
        "subjectid": 3,
        "age": 20,
        "sex": "M",
        "bmi": 19
    }

    correct_return = [s1, s2, s3]
    assert all([True if list_of_datas[i] == corr_item else False for i, corr_item in enumerate(correct_return)])


def test_example_task_data():
    """Task data is structured in some tabular format, and should be indexed by subject, visit, session"""
    list_of_datas, _ = dispatch_to_parser(
        med_api=med_api,
        filepath=os.path.join(os.path.dirname(__file__), "testfiles/testexampletask1.xlsx"),
        fileformat="tabular")

    line1 = {
        "subjectid": 1.0,
        "visitid": 1.0,
        "sessionid": 1.0,
        "accuracy": 0.9,
        "rt": 30.0
    }

    line2 = {
        "subjectid": 1.0,
        "visitid": 2.0,
        "sessionid": 1.0,
        "accuracy": 0.8,
        "rt": 50.0
    }

    line3 = {
        "subjectid": 2.0,
        "visitid": 1.0,
        "sessionid": 1.0,
        "accuracy": 0.5,
        "rt": 20.0
    }

    line4 = {
        "subjectid": 2.0,
        "visitid": 2.0,
        "sessionid": 1.0,
        "accuracy": 0.8,
        "rt": 50.0
    }

    correct_return = [line1, line2, line3, line4]

    assert (all([True if list_of_datas[i] == corr_item else False for i, corr_item in enumerate(correct_return)]))


def test_hume1_scorefile():
    study_settings_file = os.path.join(os.path.dirname(__file__),
                                       "testfiles/study_settings/MednickHumeType_study_settings.yaml")
    med_api.set_study_settings(study_settings_file)

    dict_out = dispatch_to_parser(
        med_api=med_api,
        filepath=os.path.join(os.path.dirname(__file__), "testfiles/humetype1_scorefile.mat"),
        fileformat='sleep_scoring',
        versionid=1,
        studyid="GSF")[0][0]

    correct_return = {'mins_in_waso': 0.0, 'mins_in_n1': 1.0, 'mins_in_n2': 2.0,
                      'mins_in_n3': 1.0, 'mins_in_rem': 0, 'sleep_efficiency': 1.0, 'total_sleep_time_mins': 4.0,
                      'sleep_latency_mins': 1.0, 'num_awakenings': 0, 'trans_prob_from_waso': [None, None, None, None, None],
                      'trans_prob_from_n1': [0.0, 0.0, 1.0, 0.0, 0.0],
                      'trans_prob_from_n2': [0.0, 0.0, 0.0, 1.0, 0.0],
                      'trans_prob_from_n3': [0.0, 0.0, 1.0, 0.0, 0.0],
                      'trans_prob_from_rem': [None, None, None, None, None], }

    assert all([dict_out[k] == correct_return[k] for k in correct_return.keys()])


def test_hume2_scorefile():
    study_settings_file = os.path.join(os.path.dirname(__file__),
                                       "testfiles/study_settings/MednickHumeType_study_settings.yaml")
    med_api.set_study_settings(study_settings_file)

    dict_out = dispatch_to_parser(
        med_api=med_api,
        filepath=os.path.join(os.path.dirname(__file__), "testfiles/humetype2_scorefile.mat"),
        fileformat='sleep_scoring',
        versionid=1,
        studyid="GSF")[0][0]

    correct_return = {'mins_in_waso': 20.5, 'mins_in_n1': 6.5, 'mins_in_n2': 308.0, 'mins_in_n3': 98.5,
                      'mins_in_rem': 158.5, 'sleep_efficiency': 0.9653716216216216, 'total_sleep_time_mins': 571.5,
                      'sleep_latency_mins': 6.5, 'num_awakenings': 32,
                      'trans_prob_from_waso': [0.0, 0.15625, 0.46875, 0.03125, 0.34375], 'trans_prob_from_n1':
                          [0.3333333333333333, 0.0, 0.6666666666666666, 0.0, 0.0], 'trans_prob_from_n2':
                          [0.3076923076923077, 0.0, 0.0, 0.5192307692307693, 0.17307692307692307],
                      'trans_prob_from_n3': [0.07142857142857142, 0.0, 0.9285714285714286, 0.0, 0.0],
                      'trans_prob_from_rem': [0.6, 0.0, 0.4, 0.0, 0.0], 'av_bout_duration_mins_waso': 0.640625,
                      'av_bout_duration_mins_n1': 1.0833333333333333,
                      'av_bout_duration_mins_n2': 5.811320754716981,
                      'av_bout_duration_mins_n3': 3.517857142857143, 'av_bout_duration_mins_rem': 7.925}
    assert all([dict_out[k] == correct_return[k] for k in correct_return.keys()])


def test_grass_scorefile():
    study_settings_file = os.path.join(os.path.dirname(__file__),
                                       "testfiles/study_settings/MednickGrassType_study_settings.yaml")
    med_api.set_study_settings(study_settings_file)

    dict_out = dispatch_to_parser(
        med_api=med_api,
        filepath=os.path.join(os.path.dirname(__file__), "testfiles/grasstype_scorefile.xls"),
        fileformat='sleep_scoring',
        versionid=1,
        studyid="ExampleStudyA")[0][0]

    correct_return = { 'mins_in_waso': 28.0, 'mins_in_n1': 10.0, 'mins_in_n2': 23.0,
                      'mins_in_n3': 34.0,
                      'mins_in_rem': 16.0, 'sleep_efficiency': 0.7477477477477478, 'total_sleep_time_mins': 83.0,
                      'sleep_latency_mins': 11.5,
                      'num_awakenings': 14,
                      'trans_prob_from_waso': [0.0, 0.9285714285714286, 0.0, 0.0, 0.07142857142857142],
                      'trans_prob_from_n1': [0.3333333333333333, 0.0, 0.5333333333333333, 0.0, 0.13333333333333333],
                      'trans_prob_from_n2': [0.625, 0.125, 0.0, 0.125, 0.125],
                      'trans_prob_from_n3': [1.0, 0.0, 0.0, 0.0, 0.0],
                      'trans_prob_from_rem': [1.0, 0.0, 0.0, 0.0, 0.0], 'av_bout_duration_mins_rem': 4.0,
                      'av_bout_duration_mins_n1': 0.6666666666666666, 'av_bout_duration_mins_n2': 2.875,
                      'av_bout_duration_mins_n3': 34.0,
                      'av_bout_duration_mins_waso': 2.0}

    assert all([dict_out[k] == correct_return[k] for k in correct_return.keys()])


def test_lat_scorefile():
    study_settings_file = os.path.join(os.path.dirname(__file__),
                                       "testfiles/study_settings/SpencerLab_study_settings.yaml")
    med_api.set_study_settings(study_settings_file)

    dict_out = dispatch_to_parser(
        med_api=med_api,
        filepath=os.path.join(os.path.dirname(__file__), "testfiles/lattype_scorefile.txt"),
        fileformat='sleep_scoring',
        versionid=1,
        studyid="SpencerLab")[0][0]

    correct_return = { 'mins_in_waso': 7.0, 'mins_in_n1': 9.5, 'mins_in_n2': 33.0,
                      'mins_in_n3': 16.5, 'mins_in_rem': 29.5, 'sleep_efficiency': 0.9267015706806283,
                      'total_sleep_time_mins': 88.5, 'sleep_latency_mins': 3.5, 'num_awakenings': 12,
                      'trans_prob_from_waso': [0.0, 0.75, 0.16666666666666666, 0.0, 0.08333333333333333],
                      'trans_prob_from_n1': [0.4375, 0.0, 0.5, 0.0, 0.0625],
                      'trans_prob_from_n2': [0.16666666666666666, 0.25, 0.0, 0.16666666666666666,
                                             0.4166666666666667],
                      'trans_prob_from_n3': [0.5, 0.0, 0.5, 0.0, 0.0],
                      'trans_prob_from_rem': [0.3333333333333333, 0.5, 0.16666666666666666, 0.0, 0.0],
                      'av_bout_duration_mins_waso': 0.5833333333333334,
                      'av_bout_duration_mins_n1': 0.59375,
                      'av_bout_duration_mins_n2': 2.75,
                      'av_bout_duration_mins_n3': 8.25,
                      'av_bout_duration_mins_rem': 4.214285714285714}

    assert all([dict_out[k] == correct_return[k] for k in correct_return.keys()])


def test_basic_scorefile():
    study_settings_file = os.path.join(os.path.dirname(__file__),
                                       "testfiles/study_settings/DinklemannLab_study_settings.yaml")
    med_api.set_study_settings(study_settings_file)

    dict_out = dispatch_to_parser(
        med_api=med_api,
        filepath=os.path.join(os.path.dirname(__file__), "testfiles/basictype_scorefile.txt"),
        fileformat='sleep_scoring',
        versionid=1,
        studyid="DinklemannLab")[0][0]

    correct_return = { 'mins_in_waso': 18.0, 'mins_in_n1': 27.5, 'mins_in_n2': 270.5,
                      'mins_in_n3': 60.5, 'mins_in_rem': 73.0, 'sleep_efficiency': 0.9599555061179088,
                      'total_sleep_time_mins': 431.5, 'sleep_latency_mins': 6.5, 'num_awakenings': 19,
                      'trans_prob_from_waso': [0.0, 1.0, 0.0, 0.0, 0.0],
                      'trans_prob_from_n1': [0.34375, 0.0, 0.625, 0.0, 0.03125],
                      'trans_prob_from_n2': [0.14583333333333334, 0.16666666666666666, 0.0, 0.5416666666666666,
                                             0.14583333333333334],
                      'trans_prob_from_n3': [0.0, 0.0, 1.0, 0.0, 0.0],
                      'trans_prob_from_rem': [0.125, 0.5, 0.375, 0.0, 0.0],
                      'av_bout_duration_mins_waso': 0.9473684210526315, 'av_bout_duration_mins_n1': 0.859375,
                      'av_bout_duration_mins_n2': 5.303921568627451, 'av_bout_duration_mins_n3': 2.326923076923077,
                      'av_bout_duration_mins_rem': 8.11111111111111}

    assert all([dict_out[k] == correct_return[k] for k in correct_return.keys()])


def test_full_scorefile():
    study_settings_file = os.path.join(os.path.dirname(__file__),
                                       "testfiles/study_settings/CAPStudy_study_settings.yaml")
    med_api.set_study_settings(study_settings_file)

    dict_out = dispatch_to_parser(
        med_api=med_api,
        filepath=os.path.join(os.path.dirname(__file__), "testfiles/fulltype_scorefile.txt"),
        fileformat='sleep_scoring',
        versionid=1,
        studyid="CAPStudy")[0][0]

    correct_return = { 'mins_in_waso': 41.0, 'mins_in_n1': 45.5, 'mins_in_n2': 241.0,
                      'mins_in_n3': 83.5, 'mins_in_rem': 89.5, 'sleep_efficiency': 0.9180819180819181,
                      'total_sleep_time_mins': 459.5, 'sleep_latency_mins': 6.5, 'num_awakenings': 20,
                      'trans_prob_from_waso': [0.0, 0.95, 0.05, 0.0, 0.0],
                      'trans_prob_from_n1': [0.16, 0.0, 0.84, 0.0, 0.0],
                      'trans_prob_from_n2': [0.3, 0.16666666666666666, 0.0, 0.3333333333333333, 0.2],
                      'trans_prob_from_n3': [0.1, 0.0, 0.9, 0.0, 0.0],
                      'trans_prob_from_rem': [1.0, 0.0, 0.0, 0.0, 0.0],
                      'av_bout_duration_mins_waso': 2.05, 'av_bout_duration_mins_n1': 1.82,
                      'av_bout_duration_mins_n2': 7.303030303030303, 'av_bout_duration_mins_n3': 8.35,
                      'av_bout_duration_mins_rem': 14.916666666666666}
    assert all([dict_out[k] == correct_return[k] for k in correct_return.keys()])


def test_XML_scorefile():
    study_settings_file = os.path.join(os.path.dirname(__file__), "testfiles/study_settings/NSRR_study_settings.yaml")
    med_api.set_study_settings(study_settings_file)

    dict_out = dispatch_to_parser(
        med_api=med_api,
        filepath=os.path.join(os.path.dirname(__file__), "testfiles/xmltype_scorefile.xml"),
        fileformat='sleep_scoring',
        versionid=1,
        studyid="NSRR")[0][0]

    correct_return = {'epochoffset_secs': 0.0, 'start_datetime': datetime.datetime(2000, 1, 1, 20, 33, 32),
                      'mins_in_waso': 28.5, 'mins_in_n1': 18.5, 'mins_in_n2': 209.5, 'mins_in_n3': 141.0,
                      'mins_in_rem': 121.0, 'sleep_efficiency': 0.9450337512054002, 'total_sleep_time_mins': 490.0,
                      'lights_off_secs': 0.0, 'lights_on_secs': 40650.0, 'sleep_latency_mins': 167.0, 'num_awakenings': 20,
                      'trans_prob_from_waso': [0.0, 0.55, 0.2, 0.05, 0.2],
                      'trans_prob_from_n1': [0.2, 0.0, 0.6, 0.0, 0.2],
                      'trans_prob_from_n2': [0.3333333333333333, 0.0, 0.0, 0.48148148148148145, 0.18518518518518517],
                      'trans_prob_from_n3': [0.14285714285714285, 0.0, 0.7857142857142857, 0.0, 0.07142857142857142],
                      'trans_prob_from_rem': [0.5, 0.25, 0.25, 0.0, 0.0], 'av_bout_duration_mins_waso': 1.425,
                      'av_bout_duration_mins_n1': 1.1666666666666667, 'av_bout_duration_mins_n2': 7.481481481481482,
                      'av_bout_duration_mins_n3': 10.071428571428571, 'av_bout_duration_mins_rem': 9.307692307692308}

    assert all([dict_out[k] == correct_return[k] for k in correct_return.keys()])


def test_edf1_scorefile():
    study_settings_file = os.path.join(os.path.dirname(__file__),
                                       "testfiles/study_settings/WamsleyLab_study_settings.yaml")
    med_api.set_study_settings(study_settings_file)

    dict_out = dispatch_to_parser(
        med_api=med_api,
        filepath=os.path.join(os.path.dirname(__file__), "testfiles/edftype1_scorefile.edf"),
        fileformat='sleep_scoring',
        versionid=1,
        studyid="WamsleyLab")[0][0]
    correct_return = {'epochoffset_secs': 0.0, 'start_datetime': datetime.datetime(2014, 7, 8, 23, 5, 9),
                      'mins_in_waso': 73.0, 'mins_in_n1': 25.5, 'mins_in_n2': 200.5, 'mins_in_n3': 132.5,
                      'mins_in_rem': 57.0, 'sleep_efficiency': 0.8505629477993859, 'total_sleep_time_mins': 415.5,
                      'lights_off_secs': 0.0, 'lights_on_secs': 32280.0, 'sleep_latency_mins': 50.5, 'num_awakenings': 20,
                      'trans_prob_from_waso': [0.0, 0.55, 0.4, 0.05, 0.0],
                      'trans_prob_from_n1': [0.375, 0.0, 0.5, 0.0, 0.125],
                      'trans_prob_from_n2': [0.3333333333333333, 0.047619047619047616, 0.0, 0.38095238095238093,
                                             0.23809523809523808],
                      'trans_prob_from_n3': [0.4444444444444444, 0.0, 0.5555555555555556, 0.0, 0.0],
                      'trans_prob_from_rem': [0.42857142857142855, 0.42857142857142855, 0.14285714285714285, 0.0, 0.0],
                      'av_bout_duration_mins_waso': 3.65, 'av_bout_duration_mins_n1': 1.40625,
                      'av_bout_duration_mins_n2': 8.931818181818182, 'av_bout_duration_mins_n3': 14.722222222222221,
                      'av_bout_duration_mins_rem': 8.142857142857142}
    assert all([dict_out[k] == correct_return[k] for k in correct_return.keys()])


def test_edf2_scorefile():
    study_settings_file = os.path.join(os.path.dirname(__file__), "testfiles/study_settings/Kemp_study_settings.yaml")
    med_api.set_study_settings(study_settings_file)

    dict_out = dispatch_to_parser(
        med_api=med_api,
        filepath=os.path.join(os.path.dirname(__file__), "testfiles/edftype2_scorefile.edf"),
        fileformat='sleep_scoring',
        versionid=1,
        studyid="Kemp")[0][0]

    correct_return = {
                      'mins_in_waso': 34.0,
                      'mins_in_n1': 29.0, 'mins_in_n2': 125.0, 'mins_in_n3': 110.0, 'mins_in_rem': 62.5,
                      'sleep_efficiency': 0.9056865464632455, 'total_sleep_time_mins': 326.5, 'sleep_latency_mins': 510.5,
                      'num_awakenings': 10, 'trans_prob_from_waso': [0.0, 0.9, 0.0, 0.1, 0.0],
                      'trans_prob_from_n1': [0.21739130434782608, 0.0, 0.6086956521739131, 0.043478260869565216,
                                             0.13043478260869565],
                      'trans_prob_from_n2': [0.025, 0.2, 0.0, 0.725, 0.05],
                      'trans_prob_from_n3': [0.03225806451612903, 0.12903225806451613, 0.8064516129032258, 0.0,
                                             0.03225806451612903],
                      'trans_prob_from_rem': [0.5, 0.3333333333333333, 0.16666666666666666, 0.0, 0.0],
                      'av_bout_duration_mins_waso': 3.4, 'av_bout_duration_mins_n1': 1.2083333333333333,
                      'av_bout_duration_mins_n2': 3.125, 'av_bout_duration_mins_n3': 3.5483870967741935,
                      'av_bout_duration_mins_rem': 10.416666666666666}

    assert all([dict_out[k] == correct_return[k] for k in correct_return.keys()])


def test_edf3_scorefile():
    study_settings_file = os.path.join(os.path.dirname(__file__), "testfiles/study_settings/MASS_study_settings.yaml")
    med_api.set_study_settings(study_settings_file)

    dict_out = dispatch_to_parser(
        med_api=med_api,
        filepath=os.path.join(os.path.dirname(__file__), "testfiles/edftype3_scorefile.edf"),
        fileformat='sleep_scoring',
        versionid=1,
        studyid="MASS")[0][0]

    correct_return = {
                      'mins_in_waso': 8.5,
                      'mins_in_n1': 15.5, 'mins_in_n2': 164.0, 'mins_in_n3': 60.0, 'mins_in_rem': 128.5,
                      'sleep_efficiency': 0.9774236387782205, 'total_sleep_time_mins': 368.0, 'sleep_latency_mins': 8.0,
                      'num_awakenings': 14,
                      'trans_prob_from_waso': [0.0, 0.7857142857142857, 0.14285714285714285, 0.0, 0.07142857142857142],
                      'trans_prob_from_n1': [0.10526315789473684, 0.0, 0.8421052631578947, 0.0, 0.05263157894736842],
                      'trans_prob_from_n2': [0.24390243902439024, 0.07317073170731707, 0.0, 0.5121951219512195,
                                             0.17073170731707318],
                      'trans_prob_from_n3': [0.0, 0.0, 0.9523809523809523, 0.0, 0.047619047619047616],
                      'trans_prob_from_rem': [0.2222222222222222, 0.4444444444444444, 0.3333333333333333, 0.0, 0.0],
                      'av_bout_duration_mins_waso': 0.6071428571428571,
                      'av_bout_duration_mins_n1': 0.8157894736842105, 'av_bout_duration_mins_n2': 4.0,
                      'av_bout_duration_mins_n3': 2.857142857142857, 'av_bout_duration_mins_rem': 12.85}

    assert all([dict_out[k] == correct_return[k] for k in correct_return.keys()])
