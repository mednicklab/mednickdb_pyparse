import os
import warnings
from typing import Union, List, Dict, Tuple
from mednickdb_pyparse.parse_scorefile import parse_scorefile
from mednickdb_pyparse.parse_edf import parse_raw_eeg_file, parse_sleep_eeg_file, parse_std_eeg_file
from mednickdb_pyparse.parse_tabular import parse_tabular_file
from mednickdb_pyparse.error_handling import DependencyError, ParseWarning, ParseError
from mednickdb_pyapi.pyapi import MednickAPI
from mednickdb_pysleep import edf_tools
import pickle
import yaml
import logging
from datetime import datetime
logger = logging.getLogger(__name__)

uploads_prefix = '' if os.environ['HOME'] is None else '/data/mednick_server/' #if not running in docker


def dispatch_to_parser(file_specifiers=None, get_files_from_server_storage=False, med_api=None, **kwargs: Union[dict, str, int]) -> Tuple[Union[List[dict], None], Union[List[dict], None]]:
    """
    Parses the file at fileinfo['filepath']. Data and meta data are extracted according to the rules specified by
    file_info['fileformat'].
    Returns a list of data dicts to be uploaded, and a list of files to be uploaded (or None ir no files).


    This function is called by default on any file upload and will run if fileformat matches any of
    ['raw_eeg', 'raw_sleep_eeg', 'std_eeg', 'sleep_scoring', 'std_sleep_eeg', 'tabular'].
    The following kwargs are required for each fileformat:
    tabular: None
    raw_eeg: studyid and versionid
    raw_sleep_eeg: studyid and versionid
    std_eeg: studyid and versionid
    std_sleep_eeg: studyid, versionid, and any other specifiers that match the level of that data so that the correct sleep_scoring can be grabbed
    sleep_scoring: studyid and versionid

    :param: file_specifiers: file_specifiers dict, with at least filepath, fileformat, studyid of the file to parse (as downloaded from database).
    :param: get_files_from_server_storage: Weather to try to load the given files from the server, or just use the given path
    :param kwargs: some of all of the individual keys and values of the file info object (i.e. **file_info).
    minimum set is filepath, fileformat, studyid. Useful when parsing without the need for a server (i.e. just extracting data from files)
    :param: get_files_from_server_storage: If files paths should be reletive to server file storage location, or where this file is run from.
    :return: data object to upload to the database, may addtionally return files to post to database also
    """
    if file_specifiers is None:
        file_specifiers = kwargs

    elif kwargs is not None:
        file_specifiers.update(kwargs)

    file_path = file_specifiers['filepath']
    if get_files_from_server_storage:
        file_path = uploads_prefix + file_path

    # get study settings #Add your fileformat here if you need the study settings to parse it
    if file_specifiers['fileformat'] in ['raw_eeg', 'raw_sleep_eeg', 'std_eeg', 'sleep_scoring', 'std_sleep_eeg']:
        study_settings = get_study_settings(file_specifiers, med_api)
        if study_settings is None:
            return None, None
    else:
        study_settings = None

    dispatch_map = {
        'study_settings': None,
        'sleep_features': None,
        'band_power': None,
        'study_description': None,
        'scoring_sleep_eeg': None,
        'raw_eeg': ['studyid', 'versionid'],
        'raw_sleep_eeg': ['studyid', 'versionid'],
        'std_eeg': ['studyid', 'versionid'],
        'std_sleep_eeg': ['studyid', 'versionid'],
        'sleep_scoring': ['studyid', 'versionid'],
        'tabular': [],
    }
    if file_specifiers['fileformat'] in dispatch_map:
        if dispatch_map[file_specifiers['fileformat']] is None:
            return None, None
        for kwarg in dispatch_map[file_specifiers['fileformat']]:
            if kwarg not in file_specifiers:
                raise ValueError('Fileformat', file_specifiers['fileformat'], 'requires', kwarg)
    else:
        warnings.warn('\nfileformat of {} {} is unknown, skipping'.format(file_specifiers['fileformat'],
                                                                          file_specifiers['filepath']), ParseWarning)
        return None, None

    try:
        dispatch_func = eval('dispatch_'+file_specifiers['fileformat'])
    except NameError:
        raise ParseError('No dispatch function for '+file_specifiers['fileformat'])

    logging.debug('Dispatching ' + file_specifiers['filepath'] + ' to ' + file_specifiers['fileformat']+' parse.')
    datas_ret, files_ret = dispatch_func(file_path,
                                         file_specifiers,
                                         study_settings,
                                         med_api=med_api)
    logging.debug('Returned ' + file_specifiers['filepath'] + ' from ' + file_specifiers['fileformat']+' parse.')

    return datas_ret, files_ret


def dispatch_raw_eeg(file_path: str, file_specifiers: dict, study_settings: dict, med_api: MednickAPI=None) -> Tuple[List[dict],List[dict]]:
    """
    Parse raw eeg file.
    :param file_path: path to file to parse
    :param file_specifiers: file specifiers for file
    :param study_settings: study settings dict
    :param med_api: api object
    :return: A list of data_dict objects like {specifier1:val1, specifier2:val2, data:{data_key:data_val}} and a list of file info objects
    """
    return dispatch_eeg_file(file_path, file_specifiers, study_settings)

def dispatch_raw_sleep_eeg(file_path: str, file_specifiers: dict, study_settings: dict, med_api: MednickAPI=None) -> Tuple[List[dict],List[dict]]:
    """
    Parse raw sleep file.
    :param file_path: path to file to parse
    :param file_specifiers: file specifiers for file
    :param study_settings: study settings dict
    :param med_api: api object
    :return: A list of data_dict objects like {specifier1:val1, specifier2:val2, data:{data_key:data_val}} and a list of file info objects
    """
    return dispatch_eeg_file(file_path, file_specifiers, study_settings)

def dispatch_std_eeg(file_path: str, file_specifiers: dict, study_settings: dict, med_api: MednickAPI=None) -> Tuple[List[dict],List[dict]]:
    """
    Parse std eeg file.
    :param file_path: path to file to parse
    :param file_specifiers: file specifiers for file
    :param study_settings: study settings dict
    :param med_api: api object
    :return: A list of data_dict objects like {specifier1:val1, specifier2:val2, data:{data_key:data_val}} and a list of file info objects
    """
    return parse_std_eeg_file(file_path, study_settings)

def dispatch_std_sleep_eeg(file_path: str, file_specifiers: dict, study_settings: dict, med_api: MednickAPI=None) -> Tuple[List[dict],List[dict]]:
    """
    Parse std sleep file.
    :param file_path: path to file to parse
    :param file_specifiers: file specifiers for file
    :param study_settings: study settings dict
    :param med_api: api object
    :return: A list of data_dict objects like {specifier1:val1, specifier2:val2, data:{data_key:data_val}} and a list of file info objects
    """
    data, files = dispatch_sleep_eeg_file(file_path, file_specifiers, study_settings, med_api)
    #If you wanted to add more functions to the std_sleep_eeg parse, then you could add them here, and append to data/files
    return data, files

def dispatch_tabular(file_path: str, file_specifiers: dict, study_settings: dict, med_api: MednickAPI=None) -> Tuple[List[dict],List[dict]]:
    """
    Parse tabular file.
    :param file_path: path to file to parse
    :param file_specifiers: file specifiers for file
    :param study_settings: study settings dict
    :param med_api: api object
    :return: A list of data_dict objects like {specifier1:val1, specifier2:val2, data:{data_key:data_val}} and a list of file info objects
    """
    return parse_tabular_file(file_path)

def dispatch_sleep_scoring(file_path: str, file_specifiers: dict, study_settings: dict, med_api: MednickAPI=None) -> Tuple[List[dict],List[dict]]:
    """
    Parse sleep scoring file.
    :param file_path: path to file to parse
    :param file_specifiers: file specifiers for file
    :param study_settings: study settings dict
    :param med_api: api object
    :return: A list of data_dict objects like {specifier1:val1, specifier2:val2, data:{data_key:data_val}} and a list of file info objects
    """
    return parse_scorefile(file_path, study_settings)

## Add your new parsing function for a new fileformat here! Must follow this template
# def dispatch_new_fileformat(file_path: str, file_specifiers: dict, study_settings: dict, med_api: MednickAPI=None) -> Tuple[List[dict],List[dict]]:
#     return call_to_your_function(file_path, study_settings) #must return [data_dict],[file_infos]


def dispatch_eeg_file(file_path: str, file_specifiers: dict, study_settings: dict, med_api: MednickAPI=None) -> Tuple[List[dict],List[dict]]:
    """
    Parse raw_eeg/raw_sleep_eeg file. Creates a standard eeg and scoring eeg file (if raw_sleep) and data for the raw_eeg file
    :param file_path: path to file to parse
    :param file_specifiers: file specifiers for file
    :param study_settings: study settings dict
    :param med_api: api object
    :return: A list of data_dict objects like {specifier1:val1, specifier2:val2, data:{data_key:data_val}} and a list of file info objects
    """
    std_data, ret_files = parse_raw_eeg_file(file_path, study_settings)
    if ret_files is not None:
        std_file = ret_files[0]
        base_file_name_parts = os.path.basename(file_specifiers['filepath']).split('__')
        base_file_name = '__'.join(base_file_name_parts[:-1])
        std_file_temppath = '/tmp/'+base_file_name + '_std.edf'
        edf_tools.write_edf_from_mne_raw_array(std_file, std_file_temppath)
        file_infos = [{'fileformat': file_specifiers['fileformat'].replace('raw', 'std'),
                         'filetype': file_specifiers['fileformat'].replace('raw', 'std'),
                         'file': open(std_file_temppath, 'rb')}]
        if 'sleep' in file_specifiers['fileformat']:
            scoring_file = ret_files[1]
            base_file_name_parts = os.path.basename(file_specifiers['filepath']).split('__')
            base_file_name = '__'.join(base_file_name_parts[:-1])
            std_file_temppath = '/tmp/' + base_file_name + '_scoring.edf'
            edf_tools.write_edf_from_mne_raw_array(scoring_file, std_file_temppath)
            sleep_file_info = {'fileformat': file_specifiers['fileformat'].replace('raw', 'scoring'),
                              'filetype': file_specifiers['fileformat'].replace('raw', 'scoring'),
                              'file': open(std_file_temppath, 'rb')}
            file_infos.append(sleep_file_info)
    else:
        file_infos = None
    return std_data, file_infos


def dispatch_sleep_eeg_file(file_path: str, file_specifiers: dict, study_settings: dict, med_api: MednickAPI=None) -> Tuple[List[dict],List[dict]]:
    """
    extracts sleep features and band power as both files (with detailed info per epoch/event) and as summarized information per stage/quartile
    :param file_path: path to file to parse
    :param file_specifiers: file specifiers for file
    :param study_settings: study settings dict
    :param med_api: api object
    :return: A list of data_dict objects like {specifier1:val1, specifier2:val2, data:{data_key:data_val}} and a list of file info objects
    """
    assert med_api, "cannot parse sleep_eeg without access to the scorefile via the med_api"

    scorefile_specifiers = file_specifiers.copy()
    scorefile_specifiers['filetype'] = 'sleep_scoring'
    base_file_name_parts = os.path.basename(file_specifiers['filepath']).split('__')
    base_file_name = '__'.join(base_file_name_parts[:-1])
    scorefile_specifiers.pop('fileformat')
    scorefile_specifiers.pop('filepath')
    scorefiles = med_api.get_data(**scorefile_specifiers, format='flat_dict')
    if len(scorefiles) <= 0:
        raise DependencyError('Could not parse sleep_eeg file'+file_specifiers['filepath']+
                        ' because a corresponding scorefile was not found')
    epochstages = scorefiles[0]['sleep_scoring.epochstages']
    lights_off_seconds = scorefiles[0]['sleep_scoring.lights_off_secs']
    lights_on_seconds = scorefiles[0]['sleep_scoring.lights_on_secs']
    data, files = parse_sleep_eeg_file(file_path, epochstages, study_settings,
                                       start_offset=lights_off_seconds,
                                       end_offset=lights_on_seconds)
    std_sleep_eeg_data, band_power_data, sleep_feature_data = data
    datas_out = []
    if sleep_feature_data is not None:
        datas_out.append(sleep_feature_data)
    if sleep_feature_data is not None:
        sleep_feature_data['filetype'] = 'sleep_features'
        datas_out.append(sleep_feature_data)
    if band_power_data is not None:
        band_power_data['filetype'] = 'band_power'
        datas_out.append(band_power_data)

    sleep_features_df, band_power_per_epoch = files
    files_out = []
    # package into stuff the pyapi can deal with
    if sleep_features_df is not None:
        new_filename = '/tmp/' + base_file_name + '_sleep_features.csv'
        sleep_features_df.to_csv(new_filename)
        sleep_features_file_info = {'fileformat': 'sleep_features',
                                    'filetype': 'sleep_features',
                                    'file': open(new_filename, 'rb')}
        files_out.append(sleep_features_file_info)
    if band_power_per_epoch is not None:
        new_filename = '/tmp/' + base_file_name + '_band_power.csv'
        band_power_per_epoch.to_csv(new_filename)
        band_power_file_info = {'fileformat': 'band_power',
                                'filetype': 'band_power',
                                'file': open(new_filename, 'rb')}
        files_out.append(band_power_file_info)

    return datas_out, files_out


def get_study_settings(file_specifiers: dict, med_api: MednickAPI) -> Union[None, dict]:
    """
    Return settings for a study by loading study_settings.yaml file [filetype=study_settings].
    Queries the file store via mednick_pyapi for this file.
    :param file_specifiers: specifiers for the current file being parsed
    :param med_api: ref to the MednickAPI
    :return: study settings
    :raises: DependencyError if study settings cannot be found
    """
    study_settings_infos = med_api.get_files(studyid=file_specifiers['studyid'],
                                             versionid=file_specifiers['versionid'],
                                             filetype='study_settings')
    if len(study_settings_infos) <= 0:
        logger.error('Could not parse '+file_specifiers['filepath']+
                      " because a corresponding study_settings file was not found in server's filestore")
        raise DependencyError('Could not parse '+file_specifiers['filepath']+
                      " because a corresponding study_settings file was not found in server's filestore")

    study_settings_info = study_settings_infos[0]
    if 'uploads' in study_settings_info['filepath']: #FIXME, hacky so we don't append for test. There must be a better way
        study_settings_info['filepath'] = uploads_prefix + study_settings_info['filepath']

    with open(study_settings_info['filepath'], 'rb') as f:
        return yaml.safe_load(f)



