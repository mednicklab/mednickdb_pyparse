FROM python:3.6.8-slim
WORKDIR /mednickdb
ADD setup_env.sh  .
ADD setup_pyparse.sh .
ADD requirements.txt .
ADD mednickdb_pyparse mednickdb_pyparse
ADD tests tests
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get -qq update && apt-get -qq install -y \
    build-essential \
    curl \
    unzip \
    wget \
    xorg \
    git \
    tzdata
ENV TZ America/Los_Angeles
RUN pip install -r requirements.txt && pip install pyedflib
RUN sh setup_env.sh
CMD sh setup_pyparse.sh
