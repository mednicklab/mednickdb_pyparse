import mne
import os
from mednickdb_pysleep import sleep_features, frequency_features, pysleep_defaults, pysleep_utils, artifact_detection, sleep_architecture, edf_tools, error_handling
from mednickdb_pyparse import pyparse_defaults
from mednickdb_pyparse.error_handling import DependencyError, ParseWarning, ParseError, EEGError
import pandas as pd
pd.set_option('mode.chained_assignment', None)
from typing import List, Tuple, Union
import re
import warnings
import wonambi.ioeeg.edf as wnbi_edf_reader
import wonambi.ioeeg.brainvision as wnbi_eeg_reader
from pathlib import Path
import numpy as np
import glob



def parse_raw_eeg_file(file_path: str, study_settings: dict):
    """
    extract metadata from file @path and return in a dictionary. Only supports EEG and EDF at present.
    TODO extend for all eeg type files that mne can process
    :param file_path: path of edf to parse
    :return:
    """

    ext = os.path.splitext(file_path)[-1].lower()
    if ext not in ['.edf','.eeg', '.vmrk', '.vhdr']:
        raise ParseError("Only .edf and .eeg are supported currently.")
    elif ext == '.edf':
        return parse_edf_type_file(file_path, study_settings)
    elif ext == '.eeg':
        return parse_eeg_type_file(file_path, study_settings)
    elif ext in ['.vhdr','.vmrk']:
        return None, None
    else:
        raise ParseError('eeg file type of '+ext+' is not supported')


def parse_std_eeg_file(path: str, study_settings: dict):

    if os.path.splitext(path)[-1].lower() != '.edf':
        NotImplementedError("Only EDFs are supported currently. More files coming.")

    try:  # edf
        std_edf = mne.io.read_raw_edf(path, stim_channel=None, verbose=False)
    except RuntimeError:  # edf+
        std_edf = mne.io.read_raw_edf(path, preload=True, stim_channel=None, verbose=False)

    wnbi_edf = wnbi_edf_reader.Edf(path)  # MNE screws up the dates, so lets not use it here

    std_eeg_data = {
        'start_datetime': wnbi_edf.hdr['start_time'],
        'sfreq': std_edf.info["sfreq"],
        'ch_names': std_edf.ch_names,
        'reference_type': 'linked_ear'
    }
    #eeg_chans = [ch for ch in list(study_settings['known_eeg_chans'].values()) if ch in std_edf.ch_names]
    if not (1 < 1e6 * np.sum(np.abs(std_edf.get_data())) / std_edf.get_data().size < 200):
        raise EEGError('average voltage in recording are above 200uV, there is probably a Voltage unit error')

    return [std_eeg_data], None


def parse_eeg_type_file(file_path, study_settings):

    # vhdr and eeg are needed..!
    base_path = file_path.split('__')[0].split('.')[0]
    base_file_path = os.path.split(base_path)[-1]
    base_symlink_path = '/tmp/'
    eeg_simple_path = base_symlink_path + base_file_path + '.eeg'
    vmrk_simple_path = base_symlink_path + base_file_path + '.vmrk'
    vhdr_simple_path = base_symlink_path + base_file_path + '.vhdr'
    eeg_path = file_path
    vhdr_path = max(glob.glob(base_path + '*.vhdr'), key=os.path.getmtime)
    vmrk_path = max(glob.glob(base_path + '*.vmrk'), key=os.path.getmtime)


    if not os.path.exists(vmrk_path):
        raise DependencyError(
            "Marker file not found on fileservers /upload folder for raw_sleep_eeg file " + vmrk_simple_path)

    if not os.path.exists(vhdr_path):
        raise DependencyError(
            "Header file not found on fileservers /upload folder for raw_sleep_eeg file " + vhdr_simple_path)

    def symlink_force(target, link_name):
        try:
            os.symlink(target, link_name)
        except FileExistsError as e:
            os.remove(link_name)
            os.symlink(target, link_name)

    symlink_force(vmrk_path, vmrk_simple_path)
    symlink_force(vhdr_path, vhdr_simple_path)
    symlink_force(eeg_path, eeg_simple_path)

    try:
        if os.path.getsize(file_path) > 5e+8:
            try:
                os.remove('mmap.dat')
            except FileNotFoundError:
                pass
            preload = 'mmap.dat'
            print('Large file detected, using memory map, this will be slow.')
        else:
            preload = True
        edf = mne.io.read_raw_brainvision(vhdr_simple_path, preload=preload)
        start_time = wnbi_eeg_reader._read_datetime(wnbi_eeg_reader._parse_ini(Path(vmrk_simple_path)))

        raw_eeg_data = {
            'start_datetime': start_time,
            'sfreq': edf.info["sfreq"],
            'ch_names': edf.ch_names,
            'reference_type':study_settings['ref_type']
        }

        if study_settings is not None:
            std_edf = standardize_edf_for_mednickdb(edf, study_settings)
            std_edf.info['meas_date'] = (raw_eeg_data['start_datetime'].timestamp(), 0)
            std_edf = create_std_edf(std_edf.copy(), study_settings)
            scoring_edf = create_scoring_edf(std_edf.copy(), study_settings)
            new_edfs = [std_edf, scoring_edf]
        else:
            raise DependencyError('std_eeg not created because study settings is missing')

    except Exception as e:
        os.remove(eeg_simple_path)
        os.remove(vhdr_simple_path)
        os.remove(vmrk_simple_path)
        raise e

    os.remove(eeg_simple_path)
    os.remove(vhdr_simple_path)
    os.remove(vmrk_simple_path)

    return [raw_eeg_data], new_edfs


def parse_edf_type_file(file_path, study_settings):

    ext = os.path.splitext(file_path)[-1].lower()
    if ext not in ['.edf', '.eeg']:
        NotImplementedError("Only EDFs are supported currently.")

    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        if os.path.getsize(file_path) > 5e+8:
            try:
                os.remove('mmap.dat')
            except FileNotFoundError:
                pass
            preload = 'mmap.dat'
            print('Large file detected, using memory map, this will be slow.')
        else:
            preload = True
        try:  # edf
            edf = mne.io.read_raw_edf(file_path, stim_channel=None, verbose=False, preload=preload)
        except RuntimeError:  # edf+
            edf = mne.io.read_raw_edf(file_path, preload=preload, stim_channel=None, verbose=False)
        wnbi_edf = wnbi_edf_reader.Edf(file_path)  # MNE screws up the dates, so lets not use it here
        start_time = wnbi_edf.hdr['start_time']

    raw_eeg_data = {
        'start_datetime': start_time,
        'sfreq': edf.info["sfreq"],
        'ch_names': edf.ch_names,
        'reference_type':study_settings['ref_type']
    }

    if study_settings is not None:
        std_edf = standardize_edf_for_mednickdb(edf, study_settings)
        std_edf.info['meas_date'] = (raw_eeg_data['start_datetime'].timestamp(), 0)
        std_edf = create_std_edf(std_edf.copy(), study_settings)
        scoring_edf = create_scoring_edf(std_edf.copy(), study_settings)
        new_edfs = [std_edf, scoring_edf]
    else:
        raise DependencyError('std_eeg not created because study settings is missing')

    return [raw_eeg_data], new_edfs

def standardize_edf_for_mednickdb(edf: mne.io.RawArray, study_settings: dict) -> mne.io.RawArray:
    """
    Standardizes an eeg/edf file for use with the mednickdb. This includes:
    - renaming, reordering and setting correct types for channels
    - setting bipolar emg, ecg to single channel (difference of ECG1 or ECG2 for example)
    :param edf: edf to standardize
    :param study_settings: study settings dict with channel information
    :return:
    """
    chan_map = study_settings['known_eeg_chans'].copy()
    type_map = {v:'eeg' for v in chan_map.values() if v not in ['M1','M2']}
    if 'known_emg_chans' in study_settings and study_settings['known_emg_chans']:
        chan_map.update(study_settings['known_emg_chans'])
        type_map.update({v: 'emg' for v in study_settings['known_emg_chans'].values()})
    if 'known_eog_chans' in study_settings and study_settings['known_eog_chans']:
        chan_map.update(study_settings['known_eog_chans'])
        type_map.update({v: 'eog' for v in study_settings['known_eog_chans'].values()})
    if 'known_ecg_chans' in study_settings and study_settings['known_ecg_chans']:
        chan_map.update(study_settings['known_ecg_chans'])
        type_map.update({v: 'ecg' for v in study_settings['known_ecg_chans'].values()})

    ## Drop unknowns and re-ref
    unknown_chans = [ch for ch in edf.ch_names if (ch not in chan_map) and (ch not in ['M1','M2'])]
    edf = edf.drop_channels(unknown_chans)
    for ch in list(chan_map.keys()):
        if ch not in edf.ch_names:
            warnings.warn('Channel '+ch+' found in study settings is missing from '+edf.filenames[0], ParseWarning)
            if chan_map[ch] in type_map:
                type_map.pop(chan_map[ch])
            for ch_set in ['known_eeg_chans','known_eog_chans','known_emg_chans','known_ecg_chans']:
                if study_settings[ch_set] is not None:
                    if ch in study_settings[ch_set]:
                        study_settings[ch_set].pop(ch)
            chan_map.pop(ch)

    edf.rename_channels(chan_map)

    # Load required by re-sample
    edf.load_data()

    #check data is good
    if not (1 < 1e6 * np.sum(np.abs(edf.get_data())) / edf.get_data().size < 200):
        raise EEGError('average voltage in recording are above 200uV or below 1uV, there is probably a Voltage unit error')

    # make file smaller!
    edf = edf.resample(pyparse_defaults.std_sfreq)

    #If chins are separate, then get difference
    if 'chin1' in edf.ch_names and 'chin2' in edf.ch_names:
        edf = mne.set_bipolar_reference(edf, ['chin1'],['chin2'], drop_refs=True, ch_name=['chin'], verbose=False)
        type_map.pop('chin1')
        type_map.pop('chin2')
        type_map['chin'] = 'emg'

    # If ECG are separate, then get difference
    if 'ECG1' in edf.ch_names and 'ECG2' in edf.ch_names:
        edf = mne.set_bipolar_reference(edf, ['ECG1'],['ECG2'], drop_refs=True, ch_name=['ECG'], verbose=False)
        type_map.pop('ECG1')
        type_map.pop('ECG2')
        type_map['ECG'] = 'ecg'


    # If LOC and ROC are not contra-ref'ed, then do it
    if 'LOC' in edf.ch_names and 'ROC' in edf.ch_names and study_settings['ref_type'] != 'contra_mastoid':
        edf = mne.set_bipolar_reference(edf, ['LOC','ROC'], ['M2','M1'], drop_refs=False, verbose=False)
        edf = edf.drop_channels(['LOC','ROC'])
        edf.rename_channels({ch: ch.split('-')[0] for ch in edf.ch_names})

    #Remap chan types:
    edf.set_channel_types(type_map)

    # Drop annotations (they are stored in the db)
    annot_chans = [ch for ch in edf.ch_names if 'EDF Annotations' in ch]
    edf = edf.drop_channels(annot_chans)

    # Reorder channels:
    edf = reorder_channels(edf, study_settings)

    return edf


def create_std_edf(edf, study_settings):
    """
    Standardizes an eeg/edf file to the std_edf fielformat. This includes:
    - reference to linked ear
    - resample to 256Hz (pyparse_defaults.std_sfreq)
    - filter (0.3-35Hz)
    :param edf: edf to create std_edf from
    :param study_settings: study settings dict with channel information
    :return:
    """
    # reref:
    chans = [ch for ch in set(study_settings['known_eeg_chans'].values()) if ch in edf.ch_names]
    std_edf, ref = edf_tools.rereference(edf, desired_ref='linked_ear', current_ref=study_settings['ref_type'],
                          pick_chans=chans)

    # resample
    std_edf = std_edf.resample(pyparse_defaults.std_sfreq)

    # filter:
    std_edf = std_edf.filter(pyparse_defaults.std_high_pass_hz,
                             pyparse_defaults.std_low_pass_hz,
                             picks=['eeg','eog','emg'])

    return std_edf


def create_scoring_edf(edf, study_settings):

    """
    Standardizes an eeg/edf file to the scoring_edf fielformat. This includes:
    - reference to contra-lateral mastoids
    - resample to 128Hz (pyparse_defaults.scoring_sfreq)
    - filter (0.3-35Hz)
    - Select channels: with only 'F3','F4','C3','C4','P3','P4','O1','O2','chin','LOC','ROC'  (and M1/M2)
    :param edf: edf to create std_edf from
    :param study_settings: study settings dict with channel information
    :return:
    """

    to_drop = [ch for ch in edf.ch_names if ch not in pyparse_defaults.scoring_chans]
    scoring_edf = edf.drop_channels(to_drop)

    scoring_edf, ref = edf_tools.rereference(scoring_edf,
                              desired_ref='contra_mastoid',
                              current_ref=study_settings['ref_type'])


    # resample to low sample rate to save space
    scoring_edf = scoring_edf.resample(pyparse_defaults.scoring_sfreq)

    # filter:
    eeg_chan_idx = [i for i, ch in enumerate(scoring_edf.ch_names) if ch in list(study_settings['known_eeg_chans'].values())]
    scoring_edf = scoring_edf.filter(pyparse_defaults.std_high_pass_hz,
                                     pyparse_defaults.std_low_pass_hz,
                                     picks=eeg_chan_idx)

    # Reorder channels:
    scoring_edf = reorder_channels(scoring_edf, study_settings)

    return scoring_edf

def reorder_channels(edf, study_settings):
    """
    Reorder eeg channels in left to right, back to front
    :param edf: edf to reorder
    :param study_settings: study settings dict
    :return:
    """
    chan_names_to_order = edf.ch_names
    known_emg_list = list(study_settings['known_emg_chans'].values()) if study_settings['known_emg_chans'] else None
    known_eog_list = list(study_settings['known_eog_chans'].values()) if study_settings['known_eog_chans'] else None
    known_ecg_list = list(study_settings['known_ecg_chans'].values()) if study_settings['known_ecg_chans'] else None

    def get_ordering(chan: str, known_emg_channels, known_eog_channels, known_ecg_channels) -> Tuple[Union[int,None], Union[int,None]]:
        """
        Define where a channel is located on the x, y plain, can be used for ordering
        Order will be (None, None) if channel is unknown or:
            for EMG: -1*(number of EMG + number of EOG channels)
            for EOG: -1*(number of EOG channels)
        :param: chan: The eeg channel name
        :return: order_list: A df with columns [Y sort order, X sort order]

        """

        order_x = [str(i) for i in range(9, 0, -2)] + ['z'] + [str(i) for i in range(0, 11, 2)]
        order_y = {'Fp': 1, 'AF': 2, 'F': 3, 'FC': 3, 'FT': 4, 'T': 5, 'C': 5, 'TP': 6, 'CP': 6, 'P': 7, 'PO': 8, 'O': 9, 'M':10}

        if known_emg_channels and chan in known_emg_channels:
            idx = known_emg_channels.index(chan) + 1000  # assuming we will never have more than 1000 eeg channels (safe af)
            return idx, idx

        if known_eog_channels and chan in known_eog_channels:
            idx = known_eog_channels.index(chan) + 2000
            return idx, idx

        if known_ecg_channels and chan in known_ecg_channels:
            idx = known_ecg_channels.index(chan) + 3000
            return idx, idx

        try:
            x = re.findall(r'[0-9z]+', chan)[0]
            y = re.findall(r'[a-yA-Y]+', chan)[0]
            return order_y[y], order_x.index(x)
        except (IndexError, ValueError, KeyError):
            return 5000, 5000

    ordering = list(map(lambda x: get_ordering(x,
                                               known_emg_list,
                                               known_eog_list,
                                               known_ecg_list),
                        edf.ch_names))

    ordering_ordered = [i for i, x in sorted(enumerate(ordering), key=lambda x: x[1:])]
    edf = edf.reorder_channels([chan_names_to_order[o] for o in ordering_ordered])
    return edf

def parse_sleep_eeg_file(filepath,
                         epochstages,
                         study_settings,
                         start_offset=None,
                         end_offset=None,
                         nighttime_split_method=pyparse_defaults.nighttime_split_method):
    """
    From a standardized edf file, extract relevant sleep features, including:
    spindles, so, rem, band power - per channel, per stage, per quartile if long rec
    :param filepath:
    :param epochstages:
    :param study_settings:
    :param start_offset:
    :param end_offset:
    :param nighttime_split_method:
    :return:
    """
    std_sleep_file_data = parse_std_eeg_file(filepath, study_settings)[0][0]

    all_eeg_chans = [v for v in study_settings['known_eeg_chans'].values() if v not in ['M1', 'M2']]
    raw_edf = mne.io.read_raw_edf(filepath, verbose=False)
    all_eeg_chans = [ch for ch in all_eeg_chans if ch in raw_edf.ch_names]

    #%% artifacts
    try:
        epochs_with_artifacts = artifact_detection.detect_artifacts(edf_filepath=filepath,
                                                                    epochstages=epochstages,
                                                                    epochoffset_secs=start_offset,
                                                                    end_offset=end_offset,
                                                                    hjorth_threshold=5,
                                                                    beta_threshold=3.5,
                                                                    delta_threshold=4,
                                                                    chans_to_consider=all_eeg_chans)
    except error_handling.EEGError as e:
        raise EEGError from e
    std_sleep_file_data['epochs_with_artifacts'] = epochs_with_artifacts
    std_sleep_file_data['percent_epochs_with_artifacts'] = 100*len(epochs_with_artifacts)/len(epochstages)

    #%% Band power
    band_power_chans = all_eeg_chans if study_settings['chans_for_band_power']=='all' else study_settings['chans_for_band_power']
    band_power_chans = [ch for ch in band_power_chans if ch in raw_edf.ch_names]
    if len(band_power_chans) == 0:
        warnings.warn('No band power channels. '+filepath+' is missing requested band power channels', ParseWarning)
        band_power_w_stage = None
    else:
        band_power = frequency_features.extract_band_power(edf_filepath=filepath,
                                                           epochoffset_secs=start_offset,
                                                           end_time=end_offset,
                                                           chans_to_consider=band_power_chans,
                                                           epoch_len=pysleep_defaults.band_power_epoch_len)
        band_power_per_epoch = frequency_features.extract_band_power_per_epoch(band_power,
                                                                               epoch_len=pysleep_defaults.epoch_len)

        band_power_w_stage = frequency_features.assign_band_power_stage(band_power_per_epoch,
                                                                        epochstages, bad_epochs=epochs_with_artifacts)

    if len(epochstages) > pyparse_defaults.max_nap_len_in_epochs:
        per_quartile = True
        if nighttime_split_method == 'quartiles':
            groupby = ['quartile', 'stage', 'chan', 'band']
            if band_power_w_stage is not None:
                band_power_w_stage, _ = pysleep_utils.assign_quartiles(band_power_w_stage, epochstages)
        else:
            if nighttime_split_method is not None:
                raise NotImplementedError('Only quartiles (or None) night split method is implemented')
    else:
        per_quartile = False
        groupby = ['stage', 'chan', 'band']

    if band_power_w_stage is not None:
        band_power_w_stage = band_power_w_stage.drop(['onset','duration'], axis=1)
        band_power_w_stage_to_mean = band_power_w_stage.loc[band_power_w_stage['stage'].isin(pysleep_defaults.stages_to_consider), :]
        power_df = band_power_w_stage_to_mean.groupby(groupby).agg(np.nanmean)
        power_df['power'] = pysleep_utils.trunc(power_df['power'], 3)
        power_df.index = ['_'.join(idx) for idx in power_df.index.values]
        band_power_data = power_df.T.iloc[0,:].to_dict()
    else:
        band_power_data = None

    mins_df = sleep_architecture.sleep_stage_architecture(epochstages,
                                                          epochs_to_ignore=epochs_with_artifacts,
                                                          return_type='dataframe',
                                                          per_quartile=per_quartile)

    #%% Sleep Features
    do_spindles = 'chans_for_spindles' in study_settings and study_settings['chans_for_spindles']
    spindle_channels = all_eeg_chans if study_settings['chans_for_spindles'] == 'all' else study_settings['chans_for_spindles']
    spindle_channels = [ch for ch in spindle_channels if ch in raw_edf.ch_names]
    if len(spindle_channels) == 0:
        warnings.warn('No spindle channels.'+filepath+' is missing requested spindle channels', ParseWarning)
        do_spindles = False

    do_slow_osc = 'chans_for_slow_osc' in study_settings and study_settings['chans_for_slow_osc']
    so_channels = all_eeg_chans if study_settings['chans_for_slow_osc'] == 'all' else study_settings['chans_for_slow_osc']
    so_channels = [ch for ch in so_channels if ch in raw_edf.ch_names]
    if len(so_channels) == 0:
        warnings.warn('No SO channels.'+filepath+' is missing requested SO channels', ParseWarning)
        do_slow_osc = False

    do_rem = 'chans_for_rems' in study_settings and study_settings['chans_for_rems']
    rem_channels = [ch for ch in ['ROC','LOC'] if ch in raw_edf.ch_names]
    if len(rem_channels) == 0:
        warnings.warn('No REM channels.'+filepath+' is missing LOC/ROC channels', ParseWarning)
        do_rem = False

    features_df = sleep_features.extract_features(edf_filepath=filepath,
                                                  epochstages=epochstages,
                                                  epochoffset_secs=start_offset,
                                                  end_offset=end_offset,
                                                  do_slow_osc=do_slow_osc,
                                                  do_spindles=do_spindles,
                                                  chans_for_spindles=spindle_channels,
                                                  chans_for_slow_osc=so_channels,
                                                  epochs_with_artifacts=epochs_with_artifacts,
                                                  do_rem=do_rem,
                                                  spindle_algo=pyparse_defaults.spindle_algo)

    #%% Extract features per quartile, stage, etc
    if features_df is None or features_df.shape[0] == 0:
        sleep_feature_data = {}
    else:
        if per_quartile:
            features_df, _ = pysleep_utils.assign_quartiles(features_df, epochstages)
            groupby = ['quartile', 'stage', 'chan', 'description']
        else:
            groupby = ['stage', 'chan', 'description']

        features_per_stage = sleep_features.sleep_feature_variables_per_stage(features_df,
                                                                              mins_in_stage_df=mins_df,
                                                                              av_across_channels=False,
                                                                              stages_to_consider=pysleep_defaults.stages_to_consider)
        sleep_feature_data = features_per_stage_to_data_dict(features_per_stage, groupby)

        if per_quartile: #if we did do it by quartile, now we need to do it without quartile, otherwise its already been done
            mins_df_no_quart = sleep_architecture.sleep_stage_architecture(epochstages,
                                                                           epochs_to_ignore=epochs_with_artifacts,
                                                                           return_type='dataframe',
                                                                           per_quartile=False)
            sleep_features_no_quart_df = features_df.drop(['quartile'], axis=1)
            groupby.remove('quartile')
            features_per_stage_no_quart = sleep_features.sleep_feature_variables_per_stage(sleep_features_no_quart_df,
                                                                                           mins_in_stage_df=mins_df_no_quart,
                                                                                           av_across_channels=False,
                                                                                           stages_to_consider=pysleep_defaults.stages_to_consider)
            sleep_feature_data_no_quart = features_per_stage_to_data_dict(features_per_stage_no_quart, groupby)

            sleep_feature_data.update(sleep_feature_data_no_quart)

    return [std_sleep_file_data, band_power_data, sleep_feature_data], [features_df, band_power_w_stage]


def features_per_stage_to_data_dict(features_per_stage, groupby):
    features_per_stage = features_per_stage.apply(lambda x: pd.to_numeric(x, errors='ignore'))
    agg_funcs = {col: np.nanmean for col in features_per_stage.columns if col not in groupby + ['count']}
    agg_funcs['count'] = np.nansum
    features_per_stage = features_per_stage.groupby(groupby).agg(agg_funcs)
    features_per_stage = features_per_stage.apply(lambda x: pysleep_utils.trunc(x, 3))
    features_per_stage.index = ['_'.join(idx) for idx in features_per_stage.index.values]
    sleep_feature_data = {}
    for col in features_per_stage.columns:
        sleep_feature_data.update(
            {k + '_' + col: v for k, v in features_per_stage.T.loc[col, :].to_dict().items() if not np.isnan(v)})
    return sleep_feature_data