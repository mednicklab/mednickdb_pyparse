"""
Default options and values for parsing.
Differs from pysleep defaults in that these settings are to do less with general sleep, and more with sac lab specifics
"""
# %% sleep arch
nighttime_split_method = 'quartiles'  # can be None (just av across whole night), "quartiles" or "cycles" - TODO cycles
max_nap_len_in_epochs = 3*60*2 #anything over 3 hrs is overnight

# %% sleep features
channels_to_extract_spindles_for = None  # None=all eeg, otherwise a list of chan names e.g. ['Cz', 'Pz']
channels_to_extract_sos_for = None  # None=all eeg, otherwise a list of chan names e.g. ['Cz', 'Pz']
channels_to_extract_rem_for = ['LOC', 'ROC']  # None=all eeg, otherwise a list of chan names e.g. ['Cz', 'Pz']
spindle_algo = 'Wamsley2012'

# %% std eeg
std_sfreq = 256
std_high_pass_hz = 0.3
std_low_pass_hz = 35

# %% scoring eeg
scoring_sfreq = 128 #low to save space
scoring_chans = ['F3','F4','C3','C4','P3','P4','O1','O2','M1','M2','chin','LOC','ROC']



