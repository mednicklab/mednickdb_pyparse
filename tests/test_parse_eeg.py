import sys, os

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(my_path + '/../mednickdb_pyparse/')
sys.path.append(my_path + '/../mednickdb_pyparse')
sys.path.append(my_path + '/../')
sys.path.append(my_path + '/')
from parse_edf import parse_raw_eeg_file, parse_sleep_eeg_file, parse_std_eeg_file
from mednickdb_pysleep.edf_tools import write_edf_from_mne_raw_array, rereference
import pyparse_defaults
import pickle
import yaml
from datetime import datetime
import mne
import pandas as pd
import numpy as np
import warnings


def test_eeg_parse(monkeypatch):

    study_settings = yaml.safe_load(
        open(os.path.join(os.path.dirname(__file__), 'testfiles/eeg_study_settings.yaml'), 'rb'))

    sleep_eeg_filepath = os.path.join(os.path.dirname(__file__), 'testfiles/eeg_file_test.eeg')

    raw_eeg_data, edfs = parse_raw_eeg_file(sleep_eeg_filepath, study_settings)
    raw_eeg_data = raw_eeg_data[0]
    std_edf = edfs[0]
    scoring_edf = edfs[1]
    assert scoring_edf.ch_names == ['F3','F4','C3','C4','P3','P4','O1','O2','M1','M2','chin']

    correct_raw_data = {'start_datetime': datetime(2016, 10, 21, 14, 33, 52, 493638), 'sfreq': 1000.0, 'ch_names': ['F3', 'F4', 'C3', 'C4', 'P3', 'P4', 'O1', 'O2', 'F7', 'F8', 'P7', 'P8', 'Pz', 'M1', 'M2', 'EMG1', 'EMG2', 'ECG1', 'ECG2'], 'reference_type': 'FPz'}

    assert all([correct_raw_data[k] == v for k, v in raw_eeg_data.items()])


def test_edf_parse():
    study_settings = yaml.safe_load(
        open(os.path.join(os.path.dirname(__file__), 'testfiles/example1_study_settings.yaml'), 'rb'))

    sleep_eeg_filepath = os.path.join(os.path.dirname(__file__), 'testfiles/example1_sleep_rec.edf')

    raw_eeg_data, edfs = parse_raw_eeg_file(sleep_eeg_filepath, study_settings)
    raw_eeg_data = raw_eeg_data[0]
    std_edf = edfs[0]
    scoring_edf = edfs[1]

    correct_raw_data = {'start_datetime': datetime(2014, 7, 8, 16, 5, 9), 'sfreq': 300.0,
                        'ch_names': ['Left Eye-A2', 'Right Eye-A1', 'C3-A2', 'C4-A1', 'Chin1-Chin2'],
                        'reference_type':'contra_mastoid'}

    assert all([correct_raw_data[k] == v for k, v in raw_eeg_data.items()])

    assert std_edf.info['sfreq'] == pyparse_defaults.std_sfreq

    assert isinstance(std_edf, mne.io.edf.edf.RawEDF)
    assert isinstance(scoring_edf, mne.io.edf.edf.RawEDF)
    assert all([ch in ['F3','F4','C3','C4','P3','P4','O1','O2','chin','LOC','ROC'] for ch in scoring_edf.ch_names])
    write_edf_from_mne_raw_array(std_edf,
                                 os.path.join(os.path.dirname(__file__), 'testfiles/example1_sleep_rec_std.edf'))
    assert os.path.exists(os.path.join(os.path.dirname(__file__), 'testfiles/example1_sleep_rec_std.edf'))

    std_eeg_filepath = os.path.join(os.path.dirname(__file__), 'testfiles/example1_sleep_rec_std.edf')
    std_eeg_data = parse_std_eeg_file(std_eeg_filepath, study_settings)[0][0]
    correct_std_data = {'start_datetime': datetime(2014, 7, 8, 16, 5, 9), 'sfreq': 256.0,
                        'ch_names': ['C3', 'C4', 'chin', 'LOC', 'ROC'],
                        'reference_type': 'linked_ear'}

    assert all([correct_std_data[k] == v for k, v in std_eeg_data.items()])


def test_parse_sleep_eeg_file():
    study_settings = yaml.safe_load(
        open(os.path.join(os.path.dirname(__file__), 'testfiles/example1_study_settings.yaml'), 'rb'))
    sleep_eeg_filepath = os.path.join(os.path.dirname(__file__), 'testfiles/example1_sleep_rec_std.edf')
    epochstages = pickle.load(
        open(os.path.join(os.path.dirname(__file__), 'testfiles/example1_epoch_stages.pkl'), 'rb'))

    datas_out, files_out = parse_sleep_eeg_file(sleep_eeg_filepath, epochstages, study_settings)
    sleep_features_df, band_power_per_epoch = files_out
    assert isinstance(sleep_features_df, pd.DataFrame)
    assert isinstance(band_power_per_epoch, pd.DataFrame)
    assert set(band_power_per_epoch.columns) == {'chan','band','stage','stage_idx','power','bad_epoch'}
    assert set(sleep_features_df.columns.tolist()).issuperset({'chan', 'stage', 'stage_idx', 'description'})
    std_sleep_data, band_power_data, sleep_features_data = datas_out

    std_sleep_data_correct = {'start_datetime': datetime(2014, 7, 8, 16, 5, 9), 'sfreq': 256.0, 'ch_names': ['C3', 'C4', 'chin', 'LOC', 'ROC'], 'reference_type': 'linked_ear', 'epochs_with_artifacts': [6, 18, 28, 40, 44, 48, 88, 89, 91, 96], 'percent_epochs_with_artifacts': 10.0}

    band_power_data_correct = {'n1_C3_SWA': 67.881, 'n1_C3_alpha': 11.083, 'n1_C3_beta': 3.549, 'n1_C3_delta': 40.329, 'n1_C3_fastsigma': 7.32, 'n1_C3_sigma': 8.548, 'n1_C3_slowsigma': 10.054, 'n1_C3_theta': 14.351, 'n1_C4_SWA': 71.079, 'n1_C4_alpha': 14.392, 'n1_C4_beta': 24.855, 'n1_C4_delta': 39.38, 'n1_C4_fastsigma': 37.367, 'n1_C4_sigma': 29.809, 'n1_C4_slowsigma': 14.008, 'n1_C4_theta': 13.005, 'n2_C3_SWA': 199.769, 'n2_C3_alpha': 14.161, 'n2_C3_beta': 1.795, 'n2_C3_delta': 105.611, 'n2_C3_fastsigma': 9.416, 'n2_C3_sigma': 10.525, 'n2_C3_slowsigma': 12.545, 'n2_C3_theta': 20.065, 'n2_C4_SWA': 199.237, 'n2_C4_alpha': 12.813, 'n2_C4_beta': 1.564, 'n2_C4_delta': 107.171, 'n2_C4_fastsigma': 8.221, 'n2_C4_sigma': 9.069, 'n2_C4_slowsigma': 10.756, 'n2_C4_theta': 20.305, 'n3_C3_SWA': 259.158, 'n3_C3_alpha': 13.877, 'n3_C3_beta': 1.088, 'n3_C3_delta': 131.677, 'n3_C3_fastsigma': 6.158, 'n3_C3_sigma': 7.379, 'n3_C3_slowsigma': 9.958, 'n3_C3_theta': 21.222, 'n3_C4_SWA': 246.731, 'n3_C4_alpha': 11.953, 'n3_C4_beta': 0.988, 'n3_C4_delta': 128.145, 'n3_C4_fastsigma': 5.085, 'n3_C4_sigma': 6.239, 'n3_C4_slowsigma': 8.504, 'n3_C4_theta': 20.052, 'rem_C3_SWA': 50.239, 'rem_C3_alpha': 11.271, 'rem_C3_beta': 2.211, 'rem_C3_delta': 31.784, 'rem_C3_fastsigma': 4.821, 'rem_C3_sigma': 5.903, 'rem_C3_slowsigma': 7.459, 'rem_C3_theta': 15.063, 'rem_C4_SWA': 40.505, 'rem_C4_alpha': 9.49, 'rem_C4_beta': 1.855, 'rem_C4_delta': 26.594, 'rem_C4_fastsigma': 4.173, 'rem_C4_sigma': 4.928, 'rem_C4_slowsigma': 6.006, 'rem_C4_theta': 13.948}

    for k in band_power_data_correct.keys():
        assert abs(band_power_data_correct[k] - band_power_data[k]) < 0.001, k

    sleep_features_data_correct = {'n1_C3_spindle_freq_peak': 26.317, 'n1_C4_spindle_freq_peak': 23.41, 'n2_C3_spindle_freq_peak': 13.558, 'n2_C4_spindle_freq_peak': 13.482, 'n3_C3_spindle_freq_peak': 13.076, 'n3_C4_spindle_freq_peak': 13.482, 'n1_C3_slow_osc_peak_uV': -32.792, 'n1_C3_spindle_peak_uV': 104.786, 'n1_C4_slow_osc_peak_uV': -34.923, 'n1_C4_spindle_peak_uV': 182.779, 'n2_C3_slow_osc_peak_uV': -51.012, 'n2_C3_spindle_peak_uV': 114.128, 'n2_C4_slow_osc_peak_uV': -50.684, 'n2_C4_spindle_peak_uV': 113.63, 'n3_C3_slow_osc_peak_uV': -56.275, 'n3_C3_spindle_peak_uV': 115.803, 'n3_C4_slow_osc_peak_uV': -55.176, 'n3_C4_spindle_peak_uV': 88.972, 'n1_C3_slow_osc_peak_time': 1.238, 'n1_C3_spindle_peak_time': 0.097, 'n1_C4_slow_osc_peak_time': 0.828, 'n1_C4_spindle_peak_time': 0.344, 'n2_C3_slow_osc_peak_time': 0.899, 'n2_C3_spindle_peak_time': 0.259, 'n2_C4_slow_osc_peak_time': 0.871, 'n2_C4_spindle_peak_time': 0.243, 'n3_C3_slow_osc_peak_time': 0.867, 'n3_C3_spindle_peak_time': 0.309, 'n3_C4_slow_osc_peak_time': 0.919, 'n3_C4_spindle_peak_time': 0.209, 'n1_C3_slow_osc_duration': 1.578, 'n1_C3_spindle_duration': 0.417, 'n1_C4_slow_osc_duration': 1.105, 'n1_C4_spindle_duration': 0.876, 'n2_C3_slow_osc_duration': 1.173, 'n2_C3_spindle_duration': 0.51, 'n2_C4_slow_osc_duration': 1.124, 'n2_C4_spindle_duration': 0.469, 'n3_C3_slow_osc_duration': 1.095, 'n3_C3_spindle_duration': 0.56, 'n3_C4_slow_osc_duration': 1.172, 'n3_C4_spindle_duration': 0.46, 'n1_C3_slow_osc_zero_time': 0.699, 'n1_C4_slow_osc_zero_time': 0.675, 'n2_C3_slow_osc_zero_time': 0.598, 'n2_C4_slow_osc_zero_time': 0.579, 'n3_C3_slow_osc_zero_time': 0.601, 'n3_C4_slow_osc_zero_time': 0.601, 'n1_C3_slow_osc_trough_uV': 62.008, 'n1_C4_slow_osc_trough_uV': 64.775, 'n2_C3_slow_osc_trough_uV': 84.393, 'n2_C4_slow_osc_trough_uV': 80.955, 'n3_C3_slow_osc_trough_uV': 72.792, 'n3_C4_slow_osc_trough_uV': 77.522, 'n1_C3_slow_osc_trough_time': 0.386, 'n1_C4_slow_osc_trough_time': 0.261, 'n2_C3_slow_osc_trough_time': 0.237, 'n2_C4_slow_osc_trough_time': 0.218, 'n3_C3_slow_osc_trough_time': 0.291, 'n3_C4_slow_osc_trough_time': 0.255, 'n1_C3_slow_osc_coupled_before': 0.0, 'n1_C4_slow_osc_coupled_before': 0.0, 'n2_C3_slow_osc_coupled_before': 0.111, 'n2_C4_slow_osc_coupled_before': 0.119, 'n3_C3_slow_osc_coupled_before': 0.065, 'n3_C4_slow_osc_coupled_before': 0.081, 'n1_C3_slow_osc_coupled_after': 0.0, 'n1_C4_slow_osc_coupled_after': 0.0, 'n2_C3_slow_osc_coupled_after': 0.043, 'n2_C4_slow_osc_coupled_after': 0.028, 'n3_C3_slow_osc_coupled_after': 0.032, 'n3_C4_slow_osc_coupled_after': 0.016, 'n1_C3_slow_osc_density': 2.0, 'n1_C3_spindle_density': 1.0, 'n1_C4_slow_osc_density': 1.0, 'n1_C4_spindle_density': 5.0, 'n2_C3_slow_osc_density': 6.851, 'n2_C3_spindle_density': 3.872, 'n2_C4_slow_osc_density': 7.489, 'n2_C4_spindle_density': 3.191, 'n3_C3_slow_osc_density': 11.09, 'n3_C3_spindle_density': 1.454, 'n3_C4_slow_osc_density': 11.09, 'n3_C4_spindle_density': 0.909, 'n1_C3_slow_osc_count': 2.0, 'n1_C3_spindle_count': 1.0, 'n1_C4_slow_osc_count': 1.0, 'n1_C4_spindle_count': 5.0, 'n2_C3_slow_osc_count': 161.0, 'n2_C3_spindle_count': 91.0, 'n2_C4_slow_osc_count': 176.0, 'n2_C4_spindle_count': 75.0, 'n3_C3_slow_osc_count': 61.0, 'n3_C3_spindle_count': 8.0, 'n3_C4_slow_osc_count': 61.0, 'n3_C4_spindle_count': 5.0}


    for k in sleep_features_data_correct.keys():
        assert abs(sleep_features_data_correct[k] - sleep_features_data[k]) < 0.001, k

    assert std_sleep_data == std_sleep_data_correct

    os.remove(os.path.join(os.path.dirname(__file__), 'testfiles/example1_sleep_rec_std.edf'))
