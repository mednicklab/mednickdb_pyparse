#!/bin/bash

## exit if any command fails
set -e 

## create directory for matlab runtime 
if [ -d "/mcr_flag"  ]; then
	echo -e "\nMCR exists, skip installation.\n" ;
else
	if [ -d "/opt/mcr" ]; then   ## clean unsuccessful installation
		rm -rf /opt/mcr ;
	fi
	if [ -d "/mcr-install" ]; then
		rm -rf /mcr-install;
	fi
	mkdir /mcr-install
	mkdir /opt/mcr
	cd /mcr-install

## download mcr package
	wget -q http://ssd.mathworks.com/supportfiles/downloads/R2019a/Release/0/deployment_files/installer/complete/glnxa64/MATLAB_Runtime_R2019a_glnxa64.zip
	echo "wget complete"

## unzip and install
	cd /mcr-install
	unzip -q MATLAB_Runtime_R2019a_glnxa64.zip
	./install -destinationFolder /opt/mcr -agreeToLicense yes -mode silent
	cd /
	rm -rf mcr-install
	echo "rm complete" ;

## setup LD_LIBRARY_PATH

#ldd /usr/local/lib/python3.6/lib-dynload/pyexpat.cpython-36m-x86_64-linux-gnu.so
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/mcr/v96/runtime/glnxa64:/opt/mcr/v96/bin/glnxa64:/opt/mcr/v96/sys/os/glnxa64:/opt/mcr/v96/extern/bin/glnxa64
	echo "ld_lib_path init for mcr"

#printenv | grep LD_LIBRARY_PATH
#ldd /usr/local/lib/python3.6/lib-dynload/pyexpat.cpython-36m-x86_64-linux-gnu.so

## hide mcr libexpat to resolve python dynamic lib loading
	mv  /opt/mcr/v96/bin/glnxa64/libexpat.so.1  /opt/mcr/v96/bin/glnxa64/libexpat.so.1.NOFIND
	echo "ld_lib_path modified for python packages"
#ldd /usr/local/lib/python3.6/lib-dynload/pyexpat.cpython-36m-x86_64-linux-gnu.so
	mkdir /mcr_flag 
	## export var=val is non-persistent, only valid in one shell process
fi

## mcr validation
if [  ! -d "/mcr_validation" ]; then
	git clone https://bitbucket.org/mednicklab/mcr_validation.git
	cd mcr_validation
	python setup.py install
	python validate_mcr_install.py;
fi
echo "MCR is validated"
#ls /opt/mcr/v96/bin/glnxa64 | grep "expat"

## run pyparse
cd /mednickdb
python -m mednickdb_pyparse.mednickdb_auto_parse



