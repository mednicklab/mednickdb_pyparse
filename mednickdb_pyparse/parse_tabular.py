import pandas as pd
from mednickdb_pyparse.error_handling import ParseError
import re
import numpy as np
import xlrd

# Automatic mapping from tabular column names to keys that the database knows how to deal with
spell_map = {'subid':'subjectid',
             'subject':'subjectid',
             'sub':'subjectid',
             'sid':'subjectid',
             'subjectnum':'subjectid',
             'subjnum':'subjectid',
             'subj':'subjectid',
             'visit':'visitid',
             'vis':'visitid',
             'session':'sessionid',
             'taskname':'filetype',
             'task':'filetype',
             'sess':'sessionid'}


# parsing panda objects returns jason object
def parse_tabular_file(file_name):
    """
    Read in txt, csv, xls, tsv tabular file, and strip out each row as a peice of data (formated as dictionary)
    Cols are keys, and will be converted to lowercase. Special chars are not allowed, but are not currently checked TODO
    To push this information to the database, rows should contain the appropriate hierarchical specifiers, e.g. a row for subjectid, etc
    :param file: tabular filepath to parse
    :return: a dict or a list of dicts for each row of the tabular file.
    """
    df = _read_table(file_name)
    new_cols = []
    for col in df.columns:
        if isinstance(df.loc[0,col], pd.datetime) and 'datetime' not in col:
            col = col+'_datetime'
        col = _clean_header(col)
        new_cols.append(col)
    df.columns = new_cols

    output_list = _map_df_to_dict_per_row(df)
    return output_list, None


def _clean_header(hdr):
    if isinstance(hdr, str):
        if hdr in ['Response',
                   'Please choose one answer:',
                   'Please rate the following statements using the scale below.',
                   'Answer each of the questions with your best estimate or assessment.',
                   'For the purpose of this survey, one alcoholic drink is considered one can or bottle of beer, one glass of wine, one cocktail, or one shot of liquor. ']:
            hdr = ''
        hdr = hdr.replace('CHOOSE ALL THAT APPLY.','')
        hdr = hdr.replace('Here are a number of characteristics that may or may not apply to you. For example, do you agree that you are someone who likes to spend time with others? Please indicate the extent to which you agree or disagree with that statement.','')
        hdr = hdr.replace(' ','_')
        while '__' in hdr:
            hdr = hdr.replace('__','_')
        hdr = re.sub('[^A-Za-z0-9-_]+', '', hdr).lstrip('_').rstrip('_').lower()
    else:
        hdr = str(hdr)
    return hdr


def parse_surveymonkey_tabular(file_name):
    """
    Parses the xlsx file downloaded from survey monkey. This includes napi and some questionares
    :return:
    """
    try:
        df = pd.read_excel(file_name, sheet_name='AllData', header=None)
    except xlrd.biffh.XLRDError:
        df = _read_table(file_name)

    top_header = df.iloc[0,:].values
    bottom_header = df.iloc[1,:].values



    new_headers = []
    prev_a = ''
    for a,b in zip(top_header, bottom_header):
        if not isinstance(a, str) and np.isnan(a):
            new_headers.append(_clean_header(prev_a)+'_'+_clean_header(b))
        elif not isinstance(b, str) and np.isnan(b):
            new_headers.append(_clean_header(a))
            prev_a = a
        else:
            new_headers.append(_clean_header(a)+'_'+_clean_header(b))
            prev_a = a
        if len(new_headers) > 2 and new_headers[-1] == new_headers[-2]:
            new_headers[-1] = new_headers[-1]+'_num'
            new_headers[-2] = new_headers[-2]+'_txt'

    new_headers = [col.rstrip('_').lstrip('_') for col in new_headers]
    surveymonkey_irrelevent = ['respondentid','collectorid','enddate','ip_address','email_address','first_name','lastname','custom_data']
    df.columns = new_headers
    df = df.drop([col for col in df.columns if col in surveymonkey_irrelevent], axis=1)
    df = df.loc[2:,:]
    df = df.dropna(how='all', axis=1)
    df = df.dropna(how='all', axis=0)
    df = df.rename({'startdate':'start_datetime'}, axis='columns')

    output_list = _map_df_to_dict_per_row(df)
    return output_list, None


def _map_df_to_dict_per_row(df):
    output_list = []
    for idx, row_data in df.iterrows():
        if not all([isinstance(i, str) for i in row_data.index]):
            raise ParseError("Column names are not all strings")
        row_data.index = [spell_map[i.lower()] if i.lower() in spell_map else i.lower() for i in row_data.index]
        output_list.append(row_data.to_dict())
    return output_list



def _read_table(file_name):
    if '.csv' in file_name:
        return pd.read_csv(file_name)
    elif '.txt' in file_name or '.tsv' in file_name:
        return pd.read_csv(file_name, delimiter='\t')
    elif '.xls' in file_name:
        return pd.read_excel(file_name)
    else:
        raise ParseError('unknown tabular format!')

