import numpy as np
import pandas as pd
import datetime
import time
import os
import warnings
try:
    from mednickdb_pyapi import MednickAPI
except:
    warnings.warn('MednickDB API could not be imported. Some functions will not work.') #TODO this should not be needed

module_path = os.path.dirname(os.path.abspath(__file__))


def get_stagemap_from_server(med_api, studyid, versionid, file_upload_prefix):
    """
    Gets the map from for converting a scorefile's stages to the standard format used by the db. File is grabbed from servers data store.
    :param med_api: reference to mednick_api object
    :param studyid: the studyid of the file.
    :return: the stagemap,a dict which converts one stage format to another
    :raises: FileNotFoundError if file was not found on the server
    """

    stage_maps = med_api.get_files(studyid=studyid, versionid=versionid, filetype='stage_map')

    if stage_maps is None or len(stage_maps) == 0:
        raise FileNotFoundError('stagemap not found on database')

    stage_map = stage_maps[0]
    stagemap = pd.read_excel(file_upload_prefix+stage_map['filepath'],
                             converters={'mapsfrom': str, 'mapsto': str})
    stage_map = {k: v for k, v in zip(stagemap['mapsfrom'], stagemap['mapsto'])}

    return stage_map


def get_stagemap_by_studyid(file, studyid):
    """
    Gets the map from for converting a scorefile's stages to the standard format used by the db. File is grabbed from stagemaps/ dir.
    :param studyid: the studyid of the file.
    :param file: The scorefile to parse
    :return: the stagemap,a dict which converts one stage format to another
    :raises: FileNotFoundError if file was not found
    """
    if file.endswith('.mat'):
        stagemap_type = 'hume'
    elif 'MednickLab' in studyid or 'Cellini' in studyid:
        stagemap_type = 'grass'
    elif studyid in ['K01', 'SF', 'NP', 'LSD', 'PSTIM', 'SF2014']:
        stagemap_type = 'grass'
    elif file.endswith('.xml'):
        stagemap_type = 'xml'
    else:
        stagemap_type = studyid

    stagemap = pd.read_excel(module_path+'/stagemaps/' + stagemap_type + '_stagemap.xlsx',
                             converters={'mapsfrom': str, 'mapsto': str})
    stage_map = {k: v for k, v in zip(stagemap['mapsfrom'], stagemap['mapsto'])}
    return stage_map


def get_stagemap_by_name(stagemap_name):
    """
    Gets the map from for converting a scorefile's stages to the standard format used by the db. File is grabbed from stagemaps/ dir.
    :param stagemap_name: name of the stagemap to load, one of {'hume', 'xml', 'grass'} or the name of a studyid
    :raises: FileNotFoundError if file was not found
    """
    stagemap = pd.read_excel(module_path+'/stagemaps/' + stagemap_name + '_stagemap.xlsx',
                             converters={'mapsfrom': str, 'mapsto': str})
    stage_map = {k: v for k, v in zip(stagemap['mapsfrom'], stagemap['mapsto'])}
    return stage_map
