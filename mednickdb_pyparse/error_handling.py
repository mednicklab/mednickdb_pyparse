
class ParseError(BaseException):
    """Custom error when parsing cannot complete
    This should never happen. Something has gone wrong.
    """
    pass

class ParseWarning(Warning):
    """Custom warning when parsing cannot complete
    This might happen occasionally, and is not necessarily something bad happening
    """
    pass

class DependencyError(BaseException):
    """Custom error when parsing requires another file that is not present"""
    pass

class EEGError(BaseException):
    """Custom error when something is probably wrong with an eeg record"""
    pass