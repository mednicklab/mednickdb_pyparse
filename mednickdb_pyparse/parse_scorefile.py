"""# Parse score file of various formats.
# Formats will be automatically detected. Code originally written by Seehoon and Jesse.
# (***massively***) Improved by Ben Yetton."""

import numpy as np
from mednickdb_pysleep.pysleep_defaults import stages_to_consider, include_self_transitions, wake_stages_to_consider, \
    non_sleep_or_wake_stages, epoch_len, sleep_stages, wbso_stage, wase_stage, unknown_stage
from mednickdb_pysleep import sleep_architecture, sleep_dynamics, pysleep_utils, scorefiles
from mednickdb_pyparse.error_handling import ParseError
import datetime


def parse_scorefile(file, study_settings):
    """
    Parses a scorefile to a dict that can be uploaded to database
    :param file: file to parse
    :param stage_map: stagemap that maps the scorefiles stages to mednickdb stages, get either from stagesmaps/
        folder ```get_stagemap_from_server``` or from the db (```get_stagemap_by_studyid```)
    :return: dict ready to upload, with any new data extracted from file in it. Current variable extracted are:
     - epochstage: a stage by stage map of sleep e.g. [0 1 2 1 2 3]. 0=wake, 1=stage1, 2=stage2, 3=SWS, REM=4, -1=Unknown
     - sleep_efficiency: standard sleep efficiency. sleep time/(sleep + wake time)
     - total_sleep_time: total time asleep (in stages 1, 2, SWS, REM)
     - mins_in_X
    """

    stage_map = study_settings['stage_map']

    scoring_data = {}
    try:
        scoring_data['epochstages'], scoring_data['epochoffset_secs'], scoring_data['start_datetime'] = scorefiles.extract_epochstages_from_scorefile(file, stage_map)
    except ValueError as e:
        raise ParseError('Problem parsing scorefile') from e
    if all([e == unknown_stage for e in scoring_data['epochstages']]):
        raise ParseError('All stages are unknown, this is likely caused by a bad stagemap.')

    scoring_data['epochstages'] = scorefiles.score_wake_as_waso_wbso_wase(scoring_data['epochstages'], wake_base='wake', waso='waso', wbso='wbso', wase='wase', sleep_stages=sleep_stages)



    minutes_in_stage, perc_in_stage, total_mins = sleep_architecture.sleep_stage_architecture(scoring_data['epochstages'],
                                                                                              epoch_len=epoch_len,
                                                                                              stages_to_consider=stages_to_consider)

    for stage, mins in minutes_in_stage.items():
        scoring_data['mins_in_'+stage] = mins
    scoring_data['sleep_efficiency'] = sleep_architecture.sleep_efficiency(minutes_in_stage, total_mins, wake_stages=wake_stages_to_consider)
    scoring_data['total_sleep_time_mins'] = sleep_architecture.total_sleep_time(minutes_in_stage, wake_stages=wake_stages_to_consider)
    # TODO consider lights on coming from other places. See how this is implemented in actual edfs/scorefiles...
    scoring_data['lights_off_secs'], \
    scoring_data['lights_on_secs'], \
    scoring_data['sleep_latency_mins'], \
    scoring_data['epochstages'] = sleep_architecture.lights_on_off_and_sleep_latency(scoring_data['epochstages'],
                                                                                     epoch_sync_offset_seconds=scoring_data['epochoffset_secs'],
                                                                                     wbso_stage=wbso_stage,
                                                                                     wase_stage=wase_stage,
                                                                                     stages_to_consider=stages_to_consider,
                                                                                     epoch_len=epoch_len)
    if scoring_data['start_datetime'] is not None:
        scoring_data['lights_off_datetime'] = scoring_data['start_datetime'] + datetime.timedelta(seconds=float(scoring_data['lights_off_secs']))

    scoring_data['num_awakenings'] = sleep_dynamics.num_awakenings(scoring_data['epochstages'], waso_stage='waso')
    smoothed_epoch_stages = pysleep_utils.fill_unknown_stages(scoring_data['epochstages'], stages_to_fill=non_sleep_or_wake_stages)
    _, first_order, _ = sleep_dynamics.transition_counts(smoothed_epoch_stages,
                                                         count_self_trans=include_self_transitions,
                                                         normalize=True,
                                                         stages_to_consider=stages_to_consider,)
    if first_order is not None:
        for idx, from_stage in enumerate(first_order):
            scoring_data['trans_prob_from_'+stages_to_consider[idx]] = [None if np.isnan(i) else i for i in list(from_stage)]
            assert len(scoring_data['trans_prob_from_'+stages_to_consider[idx]]) == len(stages_to_consider)

    bout_durs = sleep_dynamics.bout_durations(scoring_data['epochstages'],
                                              epoch_len=epoch_len,
                                              stages_to_consider=stages_to_consider)

    for stage, durs in bout_durs.items():
        scoring_data['av_bout_duration_mins_' + stage] = np.mean(durs) if len(durs) > 0 else 0

    return [scoring_data], None
